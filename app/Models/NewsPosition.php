<?php

namespace App\Models;

use App\Http\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class NewsPosition extends Model
{
    protected $table = 'news_position';
    protected $primaryKey = 'news_position_id';

}
