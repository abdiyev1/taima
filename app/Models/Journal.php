<?php

namespace App\Models;

use App\Http\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Journal extends Model
{
    protected $table = 'journal';
    protected $primaryKey = 'journal_id';

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
}
