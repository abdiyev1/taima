<?php

namespace App\Models;

use App\Http\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class NewsRubric extends Model
{
    protected $table = 'news_rubric';
    protected $primaryKey = 'news_rubric_id';

}
