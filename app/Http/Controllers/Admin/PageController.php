<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use View;
use DB;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(Request $request)
    {
        $row = Page::where(function($query) use ($request){
            $query->where('page_name_kz','like','%' .$request->search .'%')
                ->orWhere('page_name_ru','like','%' .$request->search .'%')
                ->orWhere('page_name_en','like','%' .$request->search .'%');
        })
            ->orderBy('page_id','desc')
            ->select('page.*',
                DB::raw('DATE_FORMAT(page.created_at,"%d.%m.%Y %H:%i") as date'));

        if(isset($request->active))
            $row->where('is_show',$request->active);
        else $row->where('is_show','1');
        
        $row = $row->paginate(20);

        return  view('admin.page.page',[
            'row' => $row,
            'request' => $request
        ]);
    }

    public function create()
    {
        $row = new Page();
        
        return  view('admin.page.page-edit', [
            'title' => 'Добавить страницу',
            'row' => $row
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'page_name_ru' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.page.page-edit', [
                'title' => 'Добавить страницу',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $page = new Page();
        
        $page->page_name_ru = $request->page_name_ru;
        $page->page_text_ru = $request->page_text_ru;
        $page->page_meta_title_ru = $request->page_meta_title_ru;
        $page->page_meta_description_ru = $request->page_meta_description_ru;
        $page->page_meta_keywords_ru = $request->page_meta_keywords_ru;
        $page->page_url_ru = $request->page_url_ru;

        $page->page_name_kz = ($request->page_name_kz != '')?$request->page_name_kz:$request->page_name_ru;
        $page->page_text_kz = ($request->page_text_kz != '')?$request->page_text_kz:$request->page_text_ru;
        $page->page_meta_title_kz = ($request->page_meta_title_kz != '')?$request->page_meta_title_kz:$request->page_meta_title_ru;
        $page->page_meta_description_kz = ($request->page_meta_description_kz != '')?$request->page_meta_description_kz:$request->page_meta_description_ru;
        $page->page_meta_keywords_kz = ($request->page_meta_keywords_kz != '')?$request->page_meta_keywords_kz:$request->page_meta_keywords_ru;
        $page->page_url_kz = ($request->page_url_kz != '')?$request->page_url_kz:$request->page_url_ru;

        $page->page_name_en = ($request->page_name_en != '')?$request->page_name_en:$request->page_name_ru;
        $page->page_text_en = ($request->page_text_en != '')?$request->page_text_en:$request->page_text_ru;
        $page->page_meta_title_en = ($request->page_meta_title_en != '')?$request->page_meta_title_en:$request->page_meta_title_ru;
        $page->page_meta_description_en = ($request->page_meta_description_en != '')?$request->page_meta_description_en:$request->page_meta_description_ru;
        $page->page_meta_keywords_en = ($request->page_meta_keywords_en != '')?$request->page_meta_keywords_en:$request->page_meta_keywords_ru;
        $page->page_url_en = ($request->page_url_en != '')?$request->page_url_en:$request->page_url_ru;

        $page->page_name_qz  = Helpers::getConvertToQazLatin($page->page_name_kz);
        $page->page_text_qz  = Helpers::getConvertToQazLatin($page->page_text_kz);
        $page->page_meta_title_qz  = Helpers::getConvertToQazLatin($page->page_meta_title_kz);
        $page->page_meta_description_qz  = Helpers::getConvertToQazLatin($page->page_meta_description_kz);
        $page->page_meta_keywords_qz  = Helpers::getConvertToQazLatin($page->page_meta_keywords_kz);
        $page->page_url_qz  = $page->page_url_kz;

        $page->is_show_menu = $request->is_show_menu;
        $page->sort_num = $request->sort_num?$request->sort_num:100;
        
        $url = '';
        $page->save();
        
        return redirect('/admin/page'.$url);
    }

    public function edit($id)
    {
        $row = Page::find($id);
     
        return  view('admin.page.page-edit', [
            'title' => 'Изменить страницу',
            'row' => $row
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'page_name_ru' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.page.page-edit', [
                'title' => 'Изменить страницу',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $page = Page::find($id);
        
        $page->page_name_ru = $request->page_name_ru;
        $page->page_text_ru = $request->page_text_ru;
        $page->page_meta_title_ru = $request->page_meta_title_ru;
        $page->page_meta_description_ru = $request->page_meta_description_ru;
        $page->page_meta_keywords_ru = $request->page_meta_keywords_ru;
        $page->page_url_ru = $request->page_url_ru;

        $page->page_name_kz = ($request->page_name_kz != '')?$request->page_name_kz:$request->page_name_ru;
        $page->page_text_kz = ($request->page_text_kz != '')?$request->page_text_kz:$request->page_text_ru;
        $page->page_meta_title_kz = ($request->page_meta_title_kz != '')?$request->page_meta_title_kz:$request->page_meta_title_ru;
        $page->page_meta_description_kz = ($request->page_meta_description_kz != '')?$request->page_meta_description_kz:$request->page_meta_description_ru;
        $page->page_meta_keywords_kz = ($request->page_meta_keywords_kz != '')?$request->page_meta_keywords_kz:$request->page_meta_keywords_ru;
        $page->page_url_kz = ($request->page_url_kz != '')?$request->page_url_kz:$request->page_url_ru;

        $page->page_name_en = ($request->page_name_en != '')?$request->page_name_en:$request->page_name_ru;
        $page->page_text_en = ($request->page_text_en != '')?$request->page_text_en:$request->page_text_ru;
        $page->page_meta_title_en = ($request->page_meta_title_en != '')?$request->page_meta_title_en:$request->page_meta_title_ru;
        $page->page_meta_description_en = ($request->page_meta_description_en != '')?$request->page_meta_description_en:$request->page_meta_description_ru;
        $page->page_meta_keywords_en = ($request->page_meta_keywords_en != '')?$request->page_meta_keywords_en:$request->page_meta_keywords_ru;
        $page->page_url_en = ($request->page_url_en != '')?$request->page_url_en:$request->page_url_ru;

        $page->page_name_qz  = Helpers::getConvertToQazLatin($page->page_name_kz);
        $page->page_text_qz  = Helpers::getConvertToQazLatin($page->page_text_kz);
        $page->page_meta_title_qz  = Helpers::getConvertToQazLatin($page->page_meta_title_kz);
        $page->page_meta_description_qz  = Helpers::getConvertToQazLatin($page->page_meta_description_kz);
        $page->page_meta_keywords_qz  = Helpers::getConvertToQazLatin($page->page_meta_keywords_kz);
        $page->page_url_qz  = $page->page_url_kz;

        $page->is_show_menu = $request->is_show_menu;
        $page->sort_num = $request->sort_num?$request->sort_num:100;

        $page->save();
        
        if($request->parent_id == '') $url = '';
        else $url = '?parent_id=' .$request->parent_id;
        
        return redirect('/admin/page'.$url);
    }

    public function destroy($id)
    {
        $page = Page::find($id);
        $page->delete();
    }

    public function changeIsShow(Request $request){
        $page = Page::find($request->id);
        $page->is_show = $request->is_show;
        $page->save();
    }
}
