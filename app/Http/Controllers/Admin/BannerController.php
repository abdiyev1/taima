<?php

namespace App\Http\Controllers\Admin;

use App\Models\Banner;
use App\Models\Section;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class BannerController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        View::share('menu', 'banner');
        
        $section = Section::get();
        View::share('section', $section);
    }

    public function index(Request $request)
    {
        $row = Banner::LeftJoin('section','section.section_id','=','banner.section_id')
            ->orderBy('banner_id','desc')
            ->select('banner.*',
                'section.*'
            );

        if(isset($request->active))
            $row->where('is_show',$request->active);
        else $row->where('is_show','1');

        $row = $row->paginate(10);

        return  view('admin.banner.banner',[
            'row' => $row
        ]);
    }

    public function create()
    {
        $row = new Banner();
        $row->image = '/img/banners/banner2.png';

        return  view('admin.banner.banner-edit', [
                'title' => 'Добавить баннер',
                'row' => $row
            ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.banner.banner-edit', [
                'title' => 'Добавить баннер',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $banner = new Banner();
        $banner->banner_name = $request->banner_name;
        $banner->image = $request->image;
        $banner->position = $request->position;
        $banner->website = $request->website;
        $banner->section_id = $request->section_id;
        $banner->is_show = 1;
        $banner->save();
        return redirect('/admin/banner');
    }

    public function edit($id)
    {
        $row = Banner::find($id);
        return  view('admin.banner.banner-edit', [
            'title' => 'Изменить баннер',
            'row' => $row
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [

        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.banner.banner-edit', [
                'title' => 'Изменить баннер',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $banner = Banner::find($id);
        $banner->banner_name = $request->banner_name;
        $banner->image = $request->image;
        $banner->position = $request->position;
        $banner->website = $request->website;
        $banner->section_id = $request->section_id;
        $banner->save();
        return redirect('/admin/banner');
    }

    public function destroy($id)
    {
        $banner = Banner::find($id);
        $banner->delete();
    }

    
    public function changeIsShow(Request $request){
        $banner = Banner::find($request->id);
        $banner->is_show = $request->is_show;
        $banner->save();
    }
}
