<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers;
use App\Models\Contact;
use App\Models\ContactCategory;
use App\Models\ContactPosition;
use App\Models\ContactRubric;
use App\Models\Position;
use App\Models\Rubric;
use App\Models\Subscribe;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use View;
use DB;
use Auth;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        View::share('menu', 'contact');
    }

    public function index(Request $request)
    {
        $row = Contact::orderBy('contact_id','desc')
                    ->select('*',
                        DB::raw('DATE_FORMAT(contact.created_at,"%d.%m.%Y %H:%i") as date'));

        if(isset($request->active))
            $row->where('is_show',$request->active);
        else $row->where('is_show','1');

        if(isset($request->name) && $request->name != ''){
            $row->where(function($query) use ($request){
                $query->where('user_name','like','%' .$request->name .'%');
            });
        }

        $row = $row->paginate(20);

        return  view('admin.contact.contact',[
            'row' => $row,
            'request' => $request
        ]);
    }

    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact->delete();
    }

    public function changeIsShow(Request $request){
        $contact = Contact::find($request->id);
        $contact->is_show = $request->is_show;
        $contact->save();
    }
    
}
