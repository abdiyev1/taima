<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use View;
use DB;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        View::share('menu', 'comment');
    }

    public function index(Request $request)
    {
        $row = Comment::leftJoin('news','news.news_id','=','comment.news_id')
                       ->orderBy('comment_id','desc')
                       ->select('comment.*',
                                'news.news_name_ru',
                                'news.news_id',
                                 DB::raw('DATE_FORMAT(comment.created_at,"%d.%m.%Y %H:%i") as date'));

        if(isset($request->active))
            $row->where('comment.is_show',$request->active);
        else $row->where('comment.is_show','1');
        
        if(isset($request->news_name) && $request->news_name != ''){
            $row->where(function($query) use ($request){
                $query->where('news_name_ru','like','%' .$request->search .'%')
                    ->orWhere('news_name_kz','like','%' .$request->search .'%')
                    ->orWhere('news_name_en','like','%' .$request->search .'%');
            });
        }
        
        if(isset($request->message) && $request->message != ''){
            $row->where('message','like','%' .$request->message .'%');
        }

        if(isset($request->user_name) && $request->user_name != ''){
            $row->where('comment_user_name','like','%' .$request->user_name .'%');
        }

        if(isset($request->email) && $request->email != ''){
            $row->where('email','like','%' .$request->email .'%');
        }
        
        $row = $row->paginate(20);

        return  view('admin.comment.comment',[
            'row' => $row,
            'request' => $request
        ]);
    }


    public function destroy($id)
    {
        $comment = Comment::find($id);
        $comment->delete();
    }

    public function changeIsShow(Request $request){
        $comment = Comment::find($request->id);
        $comment->is_show = $request->is_show;
        $comment->save();
    }

}
