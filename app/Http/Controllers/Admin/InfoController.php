<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers;
use App\Models\Info;
use App\Models\InfoPosition;
use App\Models\InfoRubric;
use App\Models\Position;
use App\Models\Rubric;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use View;
use DB;
use Auth;

class InfoController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        View::share('menu', 'info');
        
    }

    public function index(Request $request)
    {
        $row = Info::orderBy('info_id','desc')->select('info.*');

        if(isset($request->active))
            $row->where('info.is_show',$request->active);
        else $row->where('info.is_show','1');

        if(isset($request->info_name) && $request->info_name != ''){
            $row->where(function($query) use ($request){
                $query->where('info_name_ru','like','%' .$request->info_name .'%')
                    ->orWhere('info_name_kz','like','%' .$request->info_name .'%')
                    ->orWhere('info_name_en','like','%' .$request->info_name .'%');
            });
        }

     

        $row = $row->paginate(20);

        return  view('admin.info.info',[
            'row' => $row,
            'request' => $request
        ]);
    }

    public function create()
    {
        $row = new Info();
      
        return  view('admin.info.info-edit', [
            'title' => 'Добавить текст',
            'row' => $row
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'info_name_ru' => 'required',
            'info_text_ru' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.info.info-edit', [
                'title' => 'Добавить текст',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $info = new Info();
        
        $info->info_name_ru  = $request->info_name_ru;
        $info->info_text_ru  = $request->info_text_ru;
        
        $info->info_name_kz  = ($request->info_name_kz != null)?$request->info_name_kz:$request->info_name_ru;
        $info->info_text_kz  = ($request->info_text_kz != null)?$request->info_text_kz:$request->info_text_ru;
       
        $info->info_name_en  = ($request->info_name_en != null)?$request->info_name_en:$request->info_name_ru;
        $info->info_text_en  = ($request->info_text_en != null)?$request->info_text_en:$request->info_text_ru;

        $info->info_name_qz  = Helpers::getConvertToQazLatin($request->info_name_kz);
        $info->info_text_qz  = Helpers::getConvertToQazLatin($request->info_text_kz);

        $info->is_show  = 1;

        $info->save();


        return redirect('/admin/info');
    }

    public function edit($id)
    {
        $row = Info::where('info_id',$id)->first();

        return  view('admin.info.info-edit', [
            'title' => 'Изменить текст',
            'row' => $row
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'info_name_ru' => 'required',
            'info_text_ru' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.info.info-edit', [
                'title' => 'Изменить текст',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $info = Info::find($id);

        $info->info_name_ru  = $request->info_name_ru;
        $info->info_text_ru  = $request->info_text_ru;

        $info->info_name_kz  = ($request->info_name_kz != null)?$request->info_name_kz:$request->info_name_ru;
        $info->info_text_kz  = ($request->info_text_kz != null)?$request->info_text_kz:$request->info_text_ru;

        $info->info_name_en  = ($request->info_name_en != null)?$request->info_name_en:$request->info_name_ru;
        $info->info_text_en  = ($request->info_text_en != null)?$request->info_text_en:$request->info_text_ru;

        $info->info_name_qz  = Helpers::getConvertToQazLatin($request->info_name_kz);
        $info->info_text_qz  = Helpers::getConvertToQazLatin($request->info_text_kz);

        $info->save();
        
        return redirect('/admin/info');
    }

    public function destroy($id)
    {
        $info = Info::find($id);
        $info->delete();
    }

    public function changeIsShow(Request $request){
        $info = Info::find($request->id);
        $info->is_show = $request->is_show;
        $info->save();
    }
}
