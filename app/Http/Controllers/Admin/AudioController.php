<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers;
use App\Models\News;
use App\Models\NewsPosition;
use App\Models\NewsRubric;
use App\Models\Position;
use App\Models\Rubric;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use View;
use DB;
use Auth;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class AudioController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        View::share('menu', 'audio');

        $rubric = Rubric::where('is_show',1)->get();
        View::share('rubric', $rubric);

        $position = Position::where('is_show',1)->get();
        View::share('position', $position);
    }

    public function index(Request $request)
    {
        $row = News::orderBy('news_id','desc')
            ->where('is_audio','1')
            ->select('news.*',
                DB::raw('DATE_FORMAT(news.created_at,"%d.%m.%Y %H:%i") as date'));

        if(isset($request->active))
            $row->where('is_show',$request->active);
        else $row->where('is_show','1');

        if(isset($request->search) && $request->search != ''){
            $row->where(function($query) use ($request){
                $query->where('news_name_ru','like','%' .$request->search .'%')
                    ->orWhere('news_name_kz','like','%' .$request->search .'%')
                    ->orWhere('news_name_en','like','%' .$request->search .'%');
            });
        }

        $row = $row->paginate(20);

        return  view('admin.audio.audio',[
            'row' => $row,
            'request' => $request
        ]);
    }

    public function create()
    {
        $row = new News();
        $row->news_image = '/img/default.png';
        $row->news_date = date("d.m.Y H:i");
        $row->news_meta_keywords_ru = 'Taima.kz, новостной портал, новости Казахстана';
        $row->news_meta_keywords_kz = 'Taima.kz, халықаралық ақпараттық портал';
        $row->news_meta_keywords_en = 'Taima.kz, news of Kazakhstan';

        return  view('admin.audio.audio-edit', [
            'title' => 'Добавить аудио',
            'row' => $row
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'news_name_ru' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.audio.audio-edit', [
                'title' => 'Добавить аудио',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $news = new News();

        $news->news_name_ru  = $request->news_name_ru;
        $news->news_text_ru  = $request->news_text_ru;
        $news->news_meta_description_ru  = $request->news_meta_description_ru;
        $news->news_meta_keywords_ru  = $request->news_meta_keywords_ru;
        $news->tag_ru  = $request->tag_ru;
        $news->author_ru  = $request->author_ru;

        $news->news_name_kz  = ($request->news_name_kz != null)?$request->news_name_kz:$request->news_name_ru;
        $news->news_text_kz  = ($request->news_text_kz != null)?$request->news_text_kz:$request->news_text_ru;
        $news->news_meta_description_kz  = ($request->news_meta_description_kz != null)?$request->news_meta_description_kz:$request->news_meta_description_ru;
        $news->news_meta_keywords_kz  = ($request->news_meta_keywords_kz != null)?$request->news_meta_keywords_kz:$request->news_meta_keywords_ru;
        $news->tag_kz  = ($request->tag_kz != null)?$request->tag_kz:$request->tag_ru;
        $news->author_kz  = ($request->author_kz != null)?$request->author_kz:$request->author_ru;

        $news->news_name_en  = ($request->news_name_en != null)?$request->news_name_en:$request->news_name_ru;
        $news->news_text_en  = ($request->news_text_en != null)?$request->news_text_en:$request->news_text_ru;
        $news->news_meta_description_en  = ($request->news_meta_description_en != null)?$request->news_meta_description_en:$request->news_meta_description_ru;
        $news->news_meta_keywords_en = ($request->news_meta_keywords_en != null)?$request->news_meta_keywords_en:$request->news_meta_keywords_ru;
        $news->tag_en  = ($request->tag_en != null)?$request->tag_en:$request->tag_ru;
        $news->author_en  = ($request->author_en != null)?$request->author_en:$request->author_ru;

        $news->news_name_qz  = Helpers::getConvertToQazLatin($request->news_name_kz);
        $news->news_text_qz  = Helpers::getConvertToQazLatin($request->news_text_kz);
        $news->news_meta_description_qz  = Helpers::getConvertToQazLatin($request->news_meta_description_kz);
        $news->news_meta_keywords_qz  = Helpers::getConvertToQazLatin($request->news_meta_keywords_kz);
        $news->tag_qz  = Helpers::getConvertToQazLatin($request->tag_kz);
        $news->author_qz  = Helpers::getConvertToQazLatin($request->author_kz);

        $news->news_image  = $request->news_image;
        $news->user_id  = Auth::user()->user_id;

        $news->is_audio  = 1;
        $news->news_icon  = $request->news_icon;

        $timestamp = strtotime($request->news_date);
        $news->news_date = date("Y-m-d H:i", $timestamp);
        $news->is_show  = 1;
        
        $news->news_desc_ru  = $this->uploadAudio($request,'ru');
        $news->news_desc_kz  = ($request->news_desc_kz != null)?$this->uploadAudio($request,'kz'):$news->news_desc_ru;
        $news->news_desc_en  = ($request->news_desc_en != null)?$this->uploadAudio($request,'en'):$news->news_desc_ru;

        if($news->news_desc_ru == 'error1' || $news->news_desc_kz == 'error1' || $news->news_desc_en == 'error1'){
            return  view('admin.audio.audio-edit', [
                'title' => 'Изменить аудио',
                'row' => (object) $request->all(),
                'error' => 'Прикрепите только файлы форматов mp3, mp4'
            ]);
        }
        elseif($news->news_desc_ru == 'error2' || $news->news_desc_kz == 'error2' || $news->news_desc_en == 'error2') {
            return  view('admin.audio.audio-edit', [
                'title' => 'Изменить аудио',
                'row' => (object) $request->all(),
                'error' => 'Максимальный размер загружаемого файла ~ 20 МБ'
            ]);
        }

        $news->save();

        return redirect('/admin/audio');
    }

    public function edit($id)
    {
        $row = News::where('news_id',$id)->select('*',DB::raw('DATE_FORMAT(news.news_date,"%d.%m.%Y %H:%i") as news_date'))->first();

        return  view('admin.audio.audio-edit', [
            'title' => 'Изменить аудио',
            'row' => $row
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'news_name_ru' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.audio.audio-edit', [
                'title' => 'Изменить аудио',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $news = News::find($id);

        $news->news_name_ru  = $request->news_name_ru;
        $news->news_text_ru  = $request->news_text_ru;
        $news->news_meta_description_ru  = $request->news_meta_description_ru;
        $news->news_meta_keywords_ru  = $request->news_meta_keywords_ru;
        $news->tag_ru  = $request->tag_ru;
        $news->author_ru  = $request->author_ru;

        $news->news_name_kz  = ($request->news_name_kz != null)?$request->news_name_kz:$request->news_name_ru;
        $news->news_text_kz  = ($request->news_text_kz != null)?$request->news_text_kz:$request->news_text_ru;
        $news->news_meta_description_kz  = ($request->news_meta_description_kz != null)?$request->news_meta_description_kz:$request->news_meta_description_ru;
        $news->news_meta_keywords_kz  = ($request->news_meta_keywords_kz != null)?$request->news_meta_keywords_kz:$request->news_meta_keywords_ru;
        $news->tag_kz  = ($request->tag_kz != null)?$request->tag_kz:$request->tag_ru;
        $news->author_kz  = ($request->author_kz != null)?$request->author_kz:$request->author_ru;

        $news->news_name_en  = ($request->news_name_en != null)?$request->news_name_en:$request->news_name_ru;
        $news->news_text_en  = ($request->news_text_en != null)?$request->news_text_en:$request->news_text_ru;
        $news->news_meta_description_en  = ($request->news_meta_description_en != null)?$request->news_meta_description_en:$request->news_meta_description_ru;
        $news->news_meta_keywords_en = ($request->news_meta_keywords_en != null)?$request->news_meta_keywords_en:$request->news_meta_keywords_ru;
        $news->tag_en  = ($request->tag_en != null)?$request->tag_en:$request->tag_ru;
        $news->author_en  = ($request->author_en != null)?$request->author_en:$request->author_ru;

        $news->news_name_qz  = Helpers::getConvertToQazLatin($request->news_name_kz);
        $news->news_text_qz  = Helpers::getConvertToQazLatin($request->news_text_kz);
        $news->news_meta_description_qz  = Helpers::getConvertToQazLatin($request->news_meta_description_kz);
        $news->news_meta_keywords_qz  = Helpers::getConvertToQazLatin($request->news_meta_keywords_kz);
        $news->tag_qz  = Helpers::getConvertToQazLatin($request->tag_kz);
        $news->author_qz  = Helpers::getConvertToQazLatin($request->author_kz);

        $news->news_image  = $request->news_image;
        $news->user_id  = Auth::user()->user_id;

        $news->news_icon  = $request->news_icon;

        $timestamp = strtotime($request->news_date);
        $news->news_date = date("Y-m-d H:i", $timestamp);

        if($request->news_desc_ru != null){
            $news->news_desc_ru  = $this->uploadAudio($request,'ru');
        }

        if($request->news_desc_kz != null){
            $news->news_desc_kz  = $this->uploadAudio($request,'kz');
        }

        if($request->news_desc_en != null){
            $news->news_desc_en  = $this->uploadAudio($request,'en');
        }

        if($news->news_desc_ru == 'error1' || $news->news_desc_kz == 'error1' || $news->news_desc_en == 'error1'){
            return  view('admin.audio.audio-edit', [
                'title' => 'Изменить аудио',
                'row' => (object) $request->all(),
                'error' => 'Прикрепите только файлы форматов mp3, mp4'
            ]);
        }
        elseif($news->news_desc_ru == 'error2' || $news->news_desc_kz == 'error2' || $news->news_desc_en == 'error2') {
            return  view('admin.audio.audio-edit', [
                'title' => 'Изменить аудио',
                'row' => (object) $request->all(),
                'error' => 'Максимальный размер загружаемого файла ~ 20 МБ'
            ]);
        }

        $news->save();



        return redirect('/admin/audio');
    }

    public function destroy($id)
    {
        $news = News::find($id);
        $news->delete();
    }

    public function changeIsShow(Request $request){
        $news = News::find($request->id);
        $news->is_show = $request->is_show;
        $news->save();
    }


    public function uploadAudio(Request $request,$lang){
        $file = $request['news_desc_'.$lang];
        $file_name = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        if ($file->getClientMimeType() != 'audio/mpeg') {
            return 'error1';
        }
        else if($file->getClientSize() > 20097152){
            return 'error2';
        }

        $destinationPath = '/audio/'.date('Y').'/'.date('m').'/'.date('d');

        $file_name = $destinationPath .'/' .$file_name;

        if(Storage::disk('audio')->exists($file_name)){
            $now = \DateTime::createFromFormat('U.u', microtime(true));
            $file_name = $destinationPath .'/' .$now->format("Hisu").'.'.$extension;
        }

        Storage::disk('audio')->put($file_name,  File::get($file));
        /*  $result['success'] = true;
          $result['file_name'] = '/file' .$file_name;*/
        return $file_name;
    }
}
