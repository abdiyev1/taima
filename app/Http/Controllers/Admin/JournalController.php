<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers;
use App\Models\Journal;
use App\Models\Position;
use App\Models\Rubric;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use View;
use DB;
use Auth;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class JournalController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        View::share('menu', 'journal');
    }

    public function index(Request $request)
    {
        $row = Journal::orderBy('journal_id','desc')
            ->select('journal.*',
                DB::raw('DATE_FORMAT(journal.created_at,"%d.%m.%Y %H:%i") as date'));

        if(isset($request->active))
            $row->where('is_show',$request->active);
        else $row->where('is_show','1');

        if(isset($request->search) && $request->search != ''){
            $row->where(function($query) use ($request){
                $query->where('journal_name_ru','like','%' .$request->search .'%')
                    ->orWhere('journal_name_kz','like','%' .$request->search .'%')
                    ->orWhere('journal_name_en','like','%' .$request->search .'%');
            });
        }

        $row = $row->paginate(20);

        return  view('admin.journal.journal',[
            'row' => $row,
            'request' => $request
        ]);
    }

    public function create()
    {
        $row = new Journal();
        $row->journal_image = '/img/default.png';
        $row->journal_date = date("d.m.Y H:i");
        
        return  view('admin.journal.journal-edit', [
            'title' => 'Добавить журнал',
            'row' => $row
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'journal_name_ru' => 'required',
            'journal_desc_ru' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.journal.journal-edit', [
                'title' => 'Добавить журнал',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $journal = new Journal();
        
        $journal->journal_name_ru  = $request->journal_name_ru;
        
        $journal->journal_name_kz  = ($request->journal_name_kz != null)?$request->journal_name_kz:$request->journal_name_ru;
     
        $journal->journal_name_en  = ($request->journal_name_en != null)?$request->journal_name_en:$request->journal_name_ru;

        $journal->journal_name_qz  = Helpers::getConvertToQazLatin($journal->journal_name_kz);

        $journal->journal_image  = $request->journal_image;
        
        $journal->is_show  = 1;

        $journal->journal_desc_ru  = $this->uploadJournal($request,'ru');
        $journal->journal_desc_kz  = ($request->journal_desc_kz != null)?$this->uploadJournal($request,'kz'):$journal->journal_desc_ru;
        $journal->journal_desc_en  = ($request->journal_desc_en != null)?$this->uploadJournal($request,'en'):$journal->journal_desc_ru;

        $journal->journal_desc_qz  = $request->journal_desc_kz;

        if($journal->journal_desc_ru == 'error1' || $journal->journal_desc_kz == 'error1' || $journal->journal_desc_en == 'error1'){
            return  view('admin.journal.journal-edit', [
                'title' => 'Изменить журнал',
                'row' => (object) $request->all(),
                'error' => 'Прикрепите только файлы форматов mp3, mp4'
            ]);
        }
        elseif($journal->journal_desc_ru == 'error2' || $journal->journal_desc_kz == 'error2' || $journal->journal_desc_en == 'error2') {
            return  view('admin.journal.journal-edit', [
                'title' => 'Изменить журнал',
                'row' => (object) $request->all(),
                'error' => 'Максимальный размер загружаемого файла ~ 20 МБ'
            ]);
        }

        $journal->save();



        return redirect('/admin/journal');
    }

    public function edit($id)
    {
        $row = Journal::where('journal_id',$id)->select('*')->first();

        return  view('admin.journal.journal-edit', [
            'title' => 'Изменить журнал',
            'row' => $row
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'journal_name_ru' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.journal.journal-edit', [
                'title' => 'Изменить журнал',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $journal = Journal::find($id);

        $journal->journal_name_ru  = $request->journal_name_ru;


        $journal->journal_name_kz  = ($request->journal_name_kz != null)?$request->journal_name_kz:$request->journal_name_ru;

        $journal->journal_name_en  = ($request->journal_name_en != null)?$request->journal_name_en:$request->journal_name_ru;

        $journal->journal_name_qz  = Helpers::getConvertToQazLatin($journal->journal_name_kz);

        $journal->journal_image  = $request->journal_image;

        if($request->journal_desc_ru != null){
            $journal->journal_desc_ru  = $this->uploadJournal($request,'ru');
        }

        if($request->journal_desc_kz != null){
            $journal->journal_desc_kz  = $this->uploadJournal($request,'kz');
            $journal->journal_desc_qz  = $this->uploadJournal($request,'kz');
        }

        if($request->journal_desc_en != null){
            $journal->journal_desc_en  = $this->uploadJournal($request,'en');
        }


        if($journal->journal_desc_ru == 'error1' || $journal->journal_desc_kz == 'error1' || $journal->journal_desc_en == 'error1'){
            return  view('admin.journal.journal-edit', [
                'title' => 'Изменить журнал',
                'row' => (object) $request->all(),
                'error' => 'Прикрепите только файлы форматов pdf'
            ]);
        }
        elseif($journal->journal_desc_ru == 'error2' || $journal->journal_desc_kz == 'error2' || $journal->journal_desc_en == 'error2') {
            return  view('admin.journal.journal-edit', [
                'title' => 'Изменить журнал',
                'row' => (object) $request->all(),
                'error' => 'Максимальный размер загружаемого файла ~ 20 МБ'
            ]);
        }

        $journal->save();


        return redirect('/admin/journal');
    }

    public function destroy($id)
    {
        $journal = Journal::find($id);
        $journal->delete();
    }

    public function changeIsShow(Request $request){
        $journal = Journal::find($request->id);
        $journal->is_show = $request->is_show;
        $journal->save();
    }
    
    
    public function uploadJournal(Request $request,$lang){
        $file = $request['journal_desc_'.$lang];

        $file_name = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        $validator = Validator::make($request->all(), [
            'file' => 'mimes:pdf'
        ]);

        if ($validator->fails()) {
            return 'error1';
        }
        else if($file->getClientSize() > 200097152){
            return 'error2';
        }

        $destinationPath = '/pdf/'.date('Y').'/'.date('m').'/'.date('d');

        $file_name = $destinationPath .'/' .$file_name;

        if(Storage::disk('image')->exists($file_name)){
            $now = \DateTime::createFromFormat('U.u', microtime(true));
            $file_name = $destinationPath .'/' .$now->format("Hisu").'.'.$extension;
        }

        Storage::disk('image')->put($file_name,  File::get($file));
      /*  $result['success'] = true;
        $result['file_name'] = '/file' .$file_name;*/
        return '/file' .$file_name;
    }
}
