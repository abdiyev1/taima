<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers;
use App\Models\Rubric;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use View;
use DB;

class RubricController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        View::share('menu', 'rubric');
    }

    public function index(Request $request)
    {
        $row = Rubric::orderBy('rubric_id','desc')
                       ->select('rubric.*',
                                 DB::raw('DATE_FORMAT(rubric.created_at,"%d.%m.%Y %H:%i") as date'));

        if(isset($request->active))
            $row->where('is_show',$request->active);
        else $row->where('is_show','1');
        
        if(isset($request->search) && $request->search != ''){
            $row->where(function($query) use ($request){
                $query->where('rubric_name_ru','like','%' .$request->search .'%')
                    ->orWhere('rubric_name_kz','like','%' .$request->search .'%')
                    ->orWhere('rubric_name_en','like','%' .$request->search .'%')
                    ->orWhere('rubric_desc_ru','like','%' .$request->search .'%');
            });
        }
        
        $row = $row->paginate(20);

        return  view('admin.rubric.rubric',[
            'row' => $row,
            'request' => $request
        ]);
    }

    public function create()
    {
        $row = new Rubric();
        $row->rubric_image = '/media/default.jpg';

        return  view('admin.rubric.rubric-edit', [
            'title' => 'Добавить рубрику',
            'row' => $row
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'rubric_name_ru' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.rubric.rubric-edit', [
                'title' => 'Добавить рубрику',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $rubric = new Rubric();
        $rubric->rubric_name_ru = $request->rubric_name_ru;
        $rubric->rubric_meta_title_ru = $request->rubric_meta_title_ru;
        $rubric->rubric_meta_description_ru = $request->rubric_meta_description_ru;
        $rubric->rubric_meta_keywords_ru = $request->rubric_meta_keywords_ru;
        $rubric->rubric_url_ru = $request->rubric_url_ru;

        $rubric->rubric_name_kz = ($request->rubric_name_kz != '')?$request->rubric_name_kz:$request->rubric_name_ru;
        $rubric->rubric_meta_title_kz = ($request->rubric_meta_title_kz != '')?$request->rubric_meta_title_kz:$request->rubric_meta_title_ru;
        $rubric->rubric_meta_description_kz = ($request->rubric_meta_description_kz != '')?$request->rubric_meta_description_kz:$request->rubric_meta_description_ru;
        $rubric->rubric_meta_keywords_kz = ($request->rubric_meta_keywords_kz != '')?$request->rubric_meta_keywords_kz:$request->rubric_meta_keywords_ru;
        $rubric->rubric_url_kz = ($request->rubric_url_kz != '')?$request->rubric_url_kz:$request->rubric_url_ru;

        $rubric->rubric_name_en = ($request->rubric_name_en != '')?$request->rubric_name_en:$request->rubric_name_ru;
        $rubric->rubric_meta_title_en = ($request->rubric_meta_title_en != '')?$request->rubric_meta_title_en:$request->rubric_meta_title_ru;
        $rubric->rubric_meta_description_en = ($request->rubric_meta_description_en != '')?$request->rubric_meta_description_en:$request->rubric_meta_description_ru;
        $rubric->rubric_meta_keywords_en = ($request->rubric_meta_keywords_en != '')?$request->rubric_meta_keywords_en:$request->rubric_meta_keywords_ru;
        $rubric->rubric_url_en = ($request->rubric_url_en != '')?$request->rubric_url_en:$request->rubric_url_ru;

        $rubric->rubric_name_qz  = Helpers::getConvertToQazLatin($request->rubric_name_kz);
        $rubric->rubric_meta_title_qz  = Helpers::getConvertToQazLatin($request->rubric_meta_title_kz);
        $rubric->rubric_meta_description_qz  = Helpers::getConvertToQazLatin($request->rubric_meta_description_kz);
        $rubric->rubric_meta_keywords_qz  = Helpers::getConvertToQazLatin($request->rubric_meta_keywords_kz);
        $rubric->rubric_url_qz = $rubric->rubric_url_kz;

        $rubric->rubric_redirect = $request->rubric_redirect;
        $rubric->is_show_menu = $request->is_show_menu;
        $rubric->sort_num = $request->sort_num?$request->sort_num:100;
        $rubric->save();
        
        return redirect('/admin/rubric');
    }

    public function edit($id)
    {
        $row = Rubric::find($id);
     
        return  view('admin.rubric.rubric-edit', [
            'title' => 'Изменить рубрику',
            'row' => $row
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'rubric_name_ru' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.rubric.rubric-edit', [
                'title' => 'Изменить рубрику',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $rubric = Rubric::find($id);
        
        $rubric->rubric_name_ru = $request->rubric_name_ru;
        $rubric->rubric_meta_title_ru = $request->rubric_meta_title_ru;
        $rubric->rubric_meta_description_ru = $request->rubric_meta_description_ru;
        $rubric->rubric_meta_keywords_ru = $request->rubric_meta_keywords_ru;
        $rubric->rubric_url_ru = $request->rubric_url_ru;

        $rubric->rubric_name_kz = ($request->rubric_name_kz != '')?$request->rubric_name_kz:$request->rubric_name_ru;
        $rubric->rubric_meta_title_kz = ($request->rubric_meta_title_kz != '')?$request->rubric_meta_title_kz:$request->rubric_meta_title_ru;
        $rubric->rubric_meta_description_kz = ($request->rubric_meta_description_kz != '')?$request->rubric_meta_description_kz:$request->rubric_meta_description_ru;
        $rubric->rubric_meta_keywords_kz = ($request->rubric_meta_keywords_kz != '')?$request->rubric_meta_keywords_kz:$request->rubric_meta_keywords_ru;
        $rubric->rubric_url_kz = ($request->rubric_url_kz != '')?$request->rubric_url_kz:$request->rubric_url_ru;

        $rubric->rubric_name_en = ($request->rubric_name_en != '')?$request->rubric_name_en:$request->rubric_name_ru;
        $rubric->rubric_meta_title_en = ($request->rubric_meta_title_en != '')?$request->rubric_meta_title_en:$request->rubric_meta_title_ru;
        $rubric->rubric_meta_description_en = ($request->rubric_meta_description_en != '')?$request->rubric_meta_description_en:$request->rubric_meta_description_ru;
        $rubric->rubric_meta_keywords_en = ($request->rubric_meta_keywords_en != '')?$request->rubric_meta_keywords_en:$request->rubric_meta_keywords_ru;
        $rubric->rubric_url_en = ($request->rubric_url_en != '')?$request->rubric_url_en:$request->rubric_url_ru;

        $rubric->rubric_name_qz  = Helpers::getConvertToQazLatin($request->rubric_name_kz);
        $rubric->rubric_meta_title_qz  = Helpers::getConvertToQazLatin($request->rubric_meta_title_kz);
        $rubric->rubric_meta_description_qz  = Helpers::getConvertToQazLatin($request->rubric_meta_description_kz);
        $rubric->rubric_meta_keywords_qz  = Helpers::getConvertToQazLatin($request->rubric_meta_keywords_kz);
        $rubric->rubric_url_qz = $rubric->rubric_url_kz;
        
        $rubric->rubric_redirect = $request->rubric_redirect;
        $rubric->is_show_menu = $request->is_show_menu;
        $rubric->sort_num = $request->sort_num?$request->sort_num:100;
        $rubric->save();

        return redirect('/admin/rubric');
    }

    public function destroy($id)
    {
        $rubric = Rubric::find($id);
        $rubric->delete();
    }

    public function changeIsShow(Request $request){
        $rubric = Rubric::find($request->id);
        $rubric->is_show = $request->is_show;
        $rubric->save();
    }
}
