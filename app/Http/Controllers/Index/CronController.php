<?php

namespace App\Http\Controllers\Index;

use App\Http\Helpers;
use App\Models\City;
use App\Models\Currency;
use App\Models\Journal;
use App\Models\News;
use App\Models\Rubric;
use App\Models\Whether;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use DB;


class CronController extends Controller
{
    public $lang = 'ru';

    public function __construct()
    {
        $this->lang = App::getLocale();
        if($this->lang == 'qz') $this->lang = 'qz';
    }

    public function getCurrency(){
        $money_rate = Helpers::getMoneyRates();

        if(isset($money_rate[0]['description'])){
            $currency = Currency::where('currency_id','>',0)->first();
            if($currency == null) $currency = new Currency();
            $currency->usd = $money_rate[0]['description'];
            $currency->euro = $money_rate[1]['description'];
            $currency->rub = $money_rate[2]['description'];
            $currency->save();
        }

        echo var_dump($money_rate);
    }

    public function getWhether(){
        $city = City::where('is_show',1)->orderBy('sort_num','asc')->get();
        foreach ($city as $key => $item){
            $info = Helpers::getWhether($item->city_api_id);
            $result = null;
            if(isset($info['temperature'])){
                $whether = Whether::where('city_id','=',$item->city_id)->first();
                if($whether == null) $whether = new Whether();
                $whether->temperature = $info['temperature'];
                $whether->image = $info['image'];
                $whether->city_id = $item->city_id;
                $whether->save();
            }
        }

        echo var_dump('ok');
    }

    public function convertToQazLatin(){
        $news = News::get();
        foreach ($news as $item){
            $news_item = News::find($item->news_id);
            $news_item->news_name_qz  = Helpers::getConvertToQazLatin($news_item->news_name_kz);
            $news_item->news_text_qz  = Helpers::getConvertToQazLatin($news_item->news_text_kz);
            $news_item->news_desc_qz  = Helpers::getConvertToQazLatin($news_item->news_desc_kz);
            $news_item->news_meta_description_qz  = Helpers::getConvertToQazLatin($news_item->news_meta_description_kz);
            $news_item->news_meta_keywords_qz  = Helpers::getConvertToQazLatin($news_item->news_meta_keywords_kz);
            $news_item->tag_qz  = Helpers::getConvertToQazLatin($news_item->tag_kz);
            $news_item->author_qz  = Helpers::getConvertToQazLatin($news_item->author_kz);
            $news_item->save();
        }

        $rubric = Rubric::get();
        foreach ($rubric as $item){
            $rubric_item = Rubric::find($item->rubric_id);
            $rubric_item->rubric_name_qz  = Helpers::getConvertToQazLatin($rubric_item->rubric_name_kz);
            $rubric_item->rubric_meta_title_qz  = Helpers::getConvertToQazLatin($rubric_item->rubric_meta_title_kz);
            $rubric_item->rubric_meta_description_qz  = Helpers::getConvertToQazLatin($rubric_item->rubric_meta_description_kz);
            $rubric_item->rubric_meta_keywords_qz  = Helpers::getConvertToQazLatin($rubric_item->rubric_meta_keywords_kz);
            $rubric_item->rubric_url_qz = $rubric_item->rubric_url_kz;
            $rubric_item->save();
        }

        $journal = Journal::get();
        foreach ($journal as $item){
            $journal_item = Journal::find($item->journal_id);
            $journal_item->journal_name_qz  = Helpers::getConvertToQazLatin($journal_item->journal_name_kz);
            $journal_item->journal_desc_qz  = $journal_item->journal_desc_kz;
            $journal_item->save();
        }
    }
}
