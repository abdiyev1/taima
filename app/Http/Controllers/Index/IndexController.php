<?php

namespace App\Http\Controllers\Index;

use App\Http\Helpers;
use App\Models\Contact;
use App\Models\News;
use App\Models\NewsPosition;
use App\Models\NewsRubric;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Cookie;

class IndexController extends Controller
{
    public $lang = 'ru';

    public function __construct()
    {
        $this->lang = App::getLocale();
        if($this->lang == 'qz') $this->lang = 'qz';
    }

    public function index(Request $request)
    {
        $latest_news_list = NewsRubric::leftJoin('news','news.news_id','=','news_rubric.news_id')
            ->leftJoin('rubric','rubric.rubric_id','=','news_rubric.rubric_id')
            ->where('news.is_show',1)
            ->where('news.deleted_at',null)
            ->select(
                'news.news_id',
                'news.is_watermark',
                'news.news_name_'.$this->lang.' as news_name',
                DB::raw('DATE_FORMAT(news.news_date,"%Y-%m-%d %H:%i") as news_date'),
                'rubric.rubric_name_'.$this->lang.' as rubric_name',
                'rubric.rubric_url_'.$this->lang.' as rubric_url',
                'news.view_count',
                DB::raw('(select count(t.comment_id) from comment t where t.is_show = 1 and t.news_id = news.news_id) as comment_count')
            )
            ->groupBy('news.news_id')
            ->orderBy('news.news_date','desc')
            ->take(25)
            ->get();

        $main_news_list = NewsPosition::leftJoin('news','news.news_id','=','news_position.news_id')
            ->where('news.is_show',1)
            ->where('news_position.position_id',2)
            ->where('news.deleted_at',null)
            ->select(
                'news.news_id',
                'news.is_watermark',
                'news.news_icon',
                DB::raw('DATE_FORMAT(news.news_date,"%Y-%m-%d %H:%i") as news_date'),
                'news.news_image',
                'news.view_count',
                DB::raw('(select count(t.comment_id) from comment t where t.is_show = 1 and t.news_id = news.news_id) as comment_count'),
                'news.news_name_'.$this->lang.' as news_name')
            ->groupBy('news.news_id')
            ->orderBy('news.news_date','desc')
            ->take(8)
            ->get();

        $main_news = NewsPosition::leftJoin('news','news.news_id','=','news_position.news_id')
            ->where('news.is_show',1)
            ->where('news_position.position_id',1)
            ->where('news.deleted_at',null)
            ->select(
                'news.news_id',
                'news.is_watermark',
                'news.news_icon',
                DB::raw('DATE_FORMAT(news.news_date,"%Y-%m-%d %H:%i") as news_date'),
                'news.news_image',
                'news.view_count',
                DB::raw('(select count(t.comment_id) from comment t where t.is_show = 1 and t.news_id = news.news_id) as comment_count'),
                'news.news_name_'.$this->lang.' as news_name')
            ->groupBy('news.news_id')
            ->orderBy('news.news_date','desc')
            ->take(1)
            ->get();

        $popular_news_list = News::where('news.is_show',1)
            ->where('news.deleted_at',null)
            ->where('news.is_audio',0)
            ->select(
                'news.news_id',
                'news.is_watermark',
                'news.news_name_'.$this->lang.' as news_name',
                DB::raw('DATE_FORMAT(news.news_date,"%Y-%m-%d %H:%i") as news_date')
            )
            ->groupBy('news.news_id')
            ->orderBy('news.view_count','desc')
            ->take(15)
            ->get();

        $video_news_list = NewsPosition::leftJoin('news','news.news_id','=','news_position.news_id')
            ->where('news.is_show',1)
            ->where('news_position.position_id',3)
            ->where('news.deleted_at',null)
            ->select(
                'news.news_id',
                'news.is_watermark',
                DB::raw('DATE_FORMAT(news.news_date,"%Y-%m-%d %H:%i") as news_date'),
                'news.news_image',
                'news.view_count',
                DB::raw('(select count(t.comment_id) from comment t where t.is_show = 1 and t.news_id = news.news_id) as comment_count'),
                'news.news_name_'.$this->lang.' as news_name')
            ->groupBy('news.news_id')
            ->orderBy('news.news_date','desc')
            ->take(8)
            ->get();

        $photo_news_list = NewsPosition::leftJoin('news','news.news_id','=','news_position.news_id')
            ->where('news.is_show',1)
            ->where('news_position.position_id',4)
            ->where('news.deleted_at',null)
            ->select(
                'news.news_id',
                'news.news_image',
                'news.is_watermark',
                'news.view_count',
                DB::raw('DATE_FORMAT(news.news_date,"%Y-%m-%d %H:%i") as news_date'),
                'news.news_text_'.$this->lang.' as news_text',
                DB::raw('(select count(t.comment_id) from comment t where t.is_show = 1 and t.news_id = news.news_id) as comment_count'),
                'news.news_name_'.$this->lang.' as news_name')
            ->groupBy('news.news_id')
            ->orderBy('news.news_date','desc')
            ->take(5)
            ->get();

        $audio_list = News::where('news.is_show',1)
            ->where('news.deleted_at',null)
            ->where('news.is_audio',1)
            ->select(
                'news.news_id',
                'news.is_watermark',
                'news.news_name_'.$this->lang.' as news_name',
                'news.news_desc_'.$this->lang.' as news_desc',
                DB::raw('DATE_FORMAT(news.news_date,"%Y-%m-%d %H:%i") as news_date')
            )
            ->groupBy('news.news_id')
            ->orderBy('news.view_count','desc')
            ->take(15)
            ->get();

        return  view('index.index.index',
            [
                'menu' => 'home',
                'row' => $request,
                'latest_news_list' => $latest_news_list,
                'main_news_list' => $main_news_list,
                'main_news' => $main_news,
                'popular_news_list' => $popular_news_list,
                'video_news_list' => $video_news_list,
                'audio_list' => $audio_list,
                'photo_news_list' => $photo_news_list
            ]);
    }


    public function showFile($url){
        $path = storage_path('app/image/' . $url);

        if (!File::exists($path)) {
            abort(404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }

    public function sendMessage(Request $request){
        $validator = Validator::make($request->all(), [
            'user_name' => 'required',
            'email' => 'required|email',
            'contact_title' => 'required',
            'contact_message' => 'required',
        ]);


        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            $result['status'] = false;
            $result['error'] = 'Вам следует указать необходимые данные';
            return $result;
        }

        $contact = new Contact();
        $contact->user_name = $request->user_name;
        $contact->email = $request->email;
        $contact->message = $request->contact_message;
        $contact->title = $request->contact_title;
        $contact->is_show = 0;
        $contact->save();

        $result['status'] = true;
        $result['message'] = 'Успешно отправлено';

        return response()->json($result);
    }

    public function getMoneyList(Request $request){
        $money_rate = Helpers::getMoneyRates();
        if(isset($money_rate[0]['description'])){
            $result['usd'] =  $money_rate[0]['description'];
            $result['euro'] = $money_rate[1]['description'];
            $result['rub'] =  $money_rate[2]['description'];
            $result['status'] = true;
        }
        else $result['status'] = false;

        return response()->json($result);
    }
}
