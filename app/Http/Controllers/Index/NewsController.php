<?php

namespace App\Http\Controllers\Index;

use App\Http\Helpers;
use App\Models\Comment;
use App\Models\News;
use App\Models\NewsPosition;
use App\Models\NewsRubric;
use App\Models\Page;
use App\Models\Rubric;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use DB;

class NewsController extends Controller
{
    public $lang = 'ru';

    public function __construct()
    {
        $this->lang = App::getLocale();
        if($this->lang == 'qz') $this->lang = 'qz';
    }

    public function showPageByUrl(Request $request,$url)
    {
        $page = Page::where('is_show',1)
            ->where(function($query) use ($url){
                $query->where('page_url_ru',$url)
                    ->orWhere('page_url_kz',$url)
                    ->orWhere('page_url_en',$url);
            })
            ->first();

        if ($page == null) return $this->showNewsById($request,$url);


        if($page['page_url_'.$this->lang] != $url) return redirect($page['page_url_'.$this->lang],301);

        return view('index.page.page-detail',
            [
                'menu' => $page['page_url_'.$this->lang],
                'page' => $page
            ]);
    }

    public function showRubricByUrl(Request $request,$url)
    {
        $rubric = Rubric::where('is_show',1)
            ->where(function($query) use ($url){
                $query->where('rubric_url_kz',$url)
                    ->orWhere('rubric_url_en',$url)
                    ->orWhere('rubric_url_ru',$url);
            })
            ->first();

        if($rubric == null) return $this->showPageByUrl($request,$url);
        else if($rubric['rubric_url_'.$this->lang] != $url) return redirect($rubric['rubric_url_'.$this->lang],301);

        $news_list = NewsRubric::leftJoin('news','news.news_id','=','news_rubric.news_id')
            ->leftJoin('rubric','rubric.rubric_id','=','news_rubric.rubric_id')
            ->where('news.is_show',1)
            ->where('news.is_audio',0)
            ->where('news.deleted_at',null)
            ->where('news_rubric.rubric_id',$rubric->rubric_id)
            ->select(
                'news.news_id',
                'news.is_watermark',
                'news.news_icon',
                DB::raw('DATE_FORMAT(news.news_date,"%Y-%m-%d %H:%i") as news_date'),
                'news.news_image',
                'news.view_count',
                DB::raw('(select count(t.comment_id) from comment t where t.is_show = 1 and t.news_id = news.news_id) as comment_count'),
                'news.news_name_'.$this->lang.' as news_name')
            ->groupBy('news.news_id')
            ->orderBy('news.news_date','desc');

        if(isset($request->is_ajax)){
            $news_list = $news_list->where('news.news_date','<',$request->last_news_date)->paginate(6);
            return  view('index.news.news-list-loop', [
                'news_list' => $news_list,
                'is_ajax' => '1'
            ]);
        }

        $news_list = $news_list->paginate(12);

        $latest_news_list = $this->getLatestNewsListContent();

        return  view('index.rubric.rubric',
            [
                'menu' => $rubric['rubric_url_'.$this->lang],
                'row' => $request,
                'rubric' => $rubric,
                'news_list' => $news_list,
                'latest_news_list' => $latest_news_list
            ]);
    }

    public function showNewsList(Request $request)
    {
        $news_list = News::where('news.is_show',1)
            ->where('news.deleted_at',null)
            ->where('news.is_audio',0)
            ->select(
                'news.news_id',
                'news.news_icon',
                'news.is_watermark',
                DB::raw('DATE_FORMAT(news.news_date,"%Y-%m-%d %H:%i") as news_date'),
                'news.news_image',
                'news.view_count',
                DB::raw('(select count(t.comment_id) from comment t where t.is_show = 1 and t.news_id = news.news_id) as comment_count'),
                'news.news_name_'.$this->lang.' as news_name')
            ->orderBy('news.news_date','desc');

        if(isset($request->tag)){
            $news_list->where('tag_'.$this->lang,'like','%'.$request->tag.'%');
        }
        if(isset($request->is_ajax)){
            $news_list = $news_list->where('news.news_date','<',$request->last_news_date)->paginate(6);
            return  view('index.news.news-list-loop', [
                'news_list' => $news_list,
                'is_ajax' => '1'
            ]);
        }

        $news_list = $news_list->paginate(12);

        $latest_news_list = $this->getLatestNewsListContent();

        return  view('index.news.news-list',
            [
                'menu' => 'news',
                'row' => $request,
                'news_list' => $news_list,
                'latest_news_list' => $latest_news_list
            ]);
    }

    public function showAudioList(Request $request)
    {
        $rubric = Rubric::where('is_show',1)->where('rubric_id',20)->first();

        if($rubric == null) abort(404);

        $news_list = News::where('news.is_show',1)
            ->where('news.deleted_at',null)
            ->where('news.is_audio',1)
            ->select(
                'news.news_id',
                'news.is_watermark',
                DB::raw('DATE_FORMAT(news.news_date,"%Y-%m-%d %H:%i") as news_date'),
                'news.news_desc_ru',
                'news.news_name_'.$this->lang.' as news_name')
            ->orderBy('news.news_date','desc');

        if(isset($request->is_ajax)){
            $news_list = $news_list->where('news.news_date','<',$request->last_news_date)->paginate(6);
            return  view('index.audio.audio-list-loop', [
                'audio_list' => $news_list,
                'is_ajax' => '1'
            ]);
        }

        $news_list = $news_list->paginate(12);

        $latest_news_list = $this->getLatestNewsListContent();

        return  view('index.audio.audio-list',
            [
                'menu' => 'audio',
                'row' => $request,
                'rubric' => $rubric,
                'audio_list' => $news_list,
                'latest_news_list' => $latest_news_list
            ]);
    }

    public function showNewsById(Request $request,$url)
    {
        $id = Helpers::getIdFromUrl($url);

        $news = News::leftJoin('news_rubric','news_rubric.news_id','=','news.news_id')
            ->leftJoin('rubric','rubric.rubric_id','=','news_rubric.rubric_id')
            ->where('news.news_id',$id)
            ->where('news.deleted_at',null)
            ->where('news.is_audio',0)
            ->where('news.is_show',1)
            ->select(
                'news.*',
                DB::raw('(select count(t.comment_id) from comment t where t.is_show = 1 and t.news_id = news.news_id) as comment_count'),
                'rubric.rubric_name_'.$this->lang.' as rubric_name',
                'rubric.rubric_id'
            )
            ->first();

        if($news == null) abort(404);

        $news_original_url = $news->news_id .'-'.Helpers::getTranslatedSlugRu($news['news_name_'.$this->lang]);

        if($news_original_url != $url) return redirect($news_original_url,301);

        $news->view_count = $news->view_count + 1;
        $news->save();

        $comment_list = Comment::leftJoin('comment as answer','answer.comment_id','=','comment.answer_comment_id')
            ->orderBy('created_at', 'asc')
            ->where('comment.is_show',1)
            ->where('comment.news_id',$news->news_id)
            ->select('comment.*',
                DB::raw('DATE_FORMAT(comment.created_at,"%d.%m.%Y") as comment_date'),
                'answer.comment_user_name as answer_comment_user_name',
                'answer.message as answer_comment_text'
            )
            ->paginate(10);


        $latest_news_list = $this->getLatestNewsListContent();

        if($news['tag_'.$this->lang] == '') $news['tag_'.$this->lang] = '###';

        $similar_news_list = News::leftJoin('news_rubric','news_rubric.news_id','=','news.news_id')
            ->leftJoin('rubric','rubric.rubric_id','=','news_rubric.rubric_id')
            ->where('news.deleted_at',null)
            ->where('news.is_audio',0)
            ->where('news.is_show',1)
            ->where('news.news_id','!=',$news->news_id)
            ->groupBy('news.news_id')
            ->orderBy('news.news_date','desc')
            ->where(function($query) use ($request,$news){
                $query->where('rubric.rubric_id','=',$news->rubric_id)->orWhere('tag_'.$this->lang,'like','%' .$request->q .'%');
            })
            ->select(
                'news.news_name_'.$this->lang.' as news_name',
                'news.news_image',
                'news.is_watermark',
                'news.view_count',
                'news.news_id',
                DB::raw('DATE_FORMAT(news.news_date,"%Y-%m-%d %H:%i") as news_date'),
                DB::raw('(select count(t.comment_id) from comment t where t.is_show = 1 and t.news_id = news.news_id) as comment_count'),
                'rubric.rubric_name_'.$this->lang.' as rubric_name'
            )
            ->take(4)
            ->get();

        return  view('index.news.news-detail',
            [
                'menu' => 'news-detail',
                'news' => $news,
                'comment_list' => $comment_list,
                'latest_news_list' => $latest_news_list,
                'similar_news_list' => $similar_news_list
            ]);
    }

    public function search(Request $request){
        $news_list = NewsRubric::leftJoin('news','news.news_id','=','news_rubric.news_id')
            ->leftJoin('rubric','rubric.rubric_id','=','news_rubric.rubric_id')
            ->where('news.is_show',1)
            ->where('news.deleted_at',null)
            ->where('news.is_audio',0)
            ->groupBy('news.news_id')
            ->select(
                'news.news_id',
                'news.is_watermark',
                'news.news_image',
                DB::raw('DATE_FORMAT(news.news_date,"%Y-%m-%d %H:%i") as news_date'),
                'news.news_name_'.$this->lang.' as news_name',
                'rubric.rubric_name_'.$this->lang.' as rubric_name',
                'rubric.rubric_url_'.$this->lang.' as rubric_url'
            )
            ->orderBy('news.news_date','desc');

        if(isset($request->q)){
            $news_list->where(function($query) use ($request){
                $query->where('news_name_'.$this->lang,'like','%' .$request->q .'%')
                    ->orWhere('tag_'.$this->lang,'like','%' .$request->q .'%')
                    ->orWhere('news_text_'.$this->lang,'like','%' .$request->q .'%');
            });
        }
        $news_list = $news_list->paginate(10);

        return  view('index.search.search',
            [
                'menu' => 'search',
                'news_list' => $news_list
            ]);
    }

    public function showVideoList(Request $request)
    {
        $rubric = Rubric::where('is_show',1)->where('rubric_id',18)->first();

        if($rubric == null) abort(404);

        $news_list = NewsRubric::leftJoin('news','news.news_id','=','news_rubric.news_id')
            ->leftJoin('rubric','rubric.rubric_id','=','news_rubric.rubric_id')
            ->where('news.is_show',1)
            ->where('news.deleted_at',null)
            ->where('news_rubric.rubric_id',18)
            ->where('news.is_audio',0)
            ->select(
                'news.news_id',
                'news.is_watermark',
                DB::raw('DATE_FORMAT(news.news_date,"%Y-%m-%d %H:%i") as news_date'),
                'news.news_desc_ru',
                'news.news_image',
                'news.view_count',
                DB::raw('(select count(t.comment_id) from comment t where t.is_show = 1 and t.news_id = news.news_id) as comment_count'),
                'news.news_name_'.$this->lang.' as news_name')
            ->orderBy('news.news_date','desc');

        if(isset($request->is_ajax)){
            $news_list = $news_list->where('news.news_date','<',$request->last_news_date)->paginate(6);
            return  view('index.video.video-list-loop', [
                'video_list' => $news_list,
                'is_ajax' => '1'
            ]);
        }

        $news_list = $news_list->paginate(12);

        $popular_video_list = NewsRubric::leftJoin('news','news.news_id','=','news_rubric.news_id')
            ->leftJoin('rubric','rubric.rubric_id','=','news_rubric.rubric_id')
            ->where('news.is_audio',0)
            ->where('news.is_show',1)
            ->where('news.deleted_at',null)
            ->where('news_rubric.rubric_id',18)
            ->select(
                'news.news_id',
                'news.is_watermark',
                DB::raw('DATE_FORMAT(news.news_date,"%Y-%m-%d %H:%i") as news_date'),
                'news.news_name_'.$this->lang.' as news_name',
                'news.view_count',
                'news.news_image',
                DB::raw('(select count(t.comment_id) from comment t where t.is_show = 1 and t.news_id = news.news_id) as comment_count')
            )
            ->groupBy('news.news_id')
            ->orderBy('news.view_count','desc')
            ->take(4)
            ->get();

        return  view('index.video.video-list',
            [
                'menu' => 'video',
                'row' => $request,
                'rubric' => $rubric,
                'video_list' => $news_list,
                'popular_video_list' => $popular_video_list
            ]);
    }

    public function getLatestNewsListContent()
    {
        $latest_news_list = NewsRubric::leftJoin('news','news.news_id','=','news_rubric.news_id')
            ->leftJoin('rubric','rubric.rubric_id','=','news_rubric.rubric_id')
            ->where('news.is_show',1)
            ->where('news.is_audio',0)
            ->where('news.deleted_at',null)
            ->groupBy('news.news_id')
            ->select(
                'news.news_id',
                'news.is_watermark',
                'news.news_name_'.$this->lang.' as news_name',
                'rubric.rubric_name_'.$this->lang.' as rubric_name',
                'rubric.rubric_url_'.$this->lang.' as rubric_url',
                DB::raw('DATE_FORMAT(news.news_date,"%Y-%m-%d %H:%i") as news_date'),
                'news.view_count',
                DB::raw('(select count(t.comment_id) from comment t where t.is_show = 1 and t.news_id = news.news_id) as comment_count')
            )
            ->groupBy('news.news_id')
            ->orderBy('news.news_date','desc')
            ->take(25)
            ->get();

        return $latest_news_list;
    }
}
