<?php

namespace App\Http\Controllers\Index;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use DB;
use Illuminate\Support\Facades\Response;

class CommentController extends Controller
{
    public $lang = 'ru';

    public function __construct()
    {
        $this->lang = App::getLocale();
        if($this->lang == 'qz') $this->lang = 'qz';
    }

    public function getCommentListByNews(Request $request)
    {
        $comment_list = Comment::leftJoin('comment as answer','answer.comment_id','=','comment.answer_comment_id')
                                ->orderBy('created_at', 'asc')
                                ->where('comment.is_show',1)
                                ->select('comment.*',
                                    DB::raw('DATE_FORMAT(comment.created_at,"%d.%m.%Y") as comment_date'),
                                    'answer.comment_user_name as answer_comment_user_name',
                                    'answer.message as answer_comment_text'
                                )
                                ->where('news_id', $request->news_id);


        if($request->last_comment_id != null){
            $comment_list->where('comment_id', '>', $request->last_comment_id);
        }

        $comment_list = $comment_list->paginate(10);
      
        return view('index.news.comment-list-loop', ['comment_list' => $comment_list,'is_ajax' => 1]);
    }

    public function addComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment_user_name' => 'required',
            'comment_email' => 'required',
            'comment_text' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            $result['error'] = $error[0];
            $result['status'] = false;
            return response()->json($result);
        }

        $comment = new Comment();
        $comment->comment_user_name = $request->comment_user_name;
        $comment->news_id = $request->news_id;
        $comment->email = $request->comment_email;
        $comment->message = $request->comment_text;
        $comment->answer_comment_id = $request->answer_comment_id;
        $comment->is_show = 0;

        try {
            $comment->save();

        } catch (\Exception $ex) {
            $result['error'] = 'Ошибка базы данных';
            $result['error_code'] = 500;
            $result['status'] = false;
            return response()->json($result);
        }

        $result['message'] = 'Спасибо за Ваш комментарий, он будет опубликован после проверки модератором!';
        $result['status'] = true;
        return response()->json($result);

    }
}
