$(document).ready(function() {
    $('.ic-menu').click(function() {
        $('.menu-box').show();
        $('.in-box-menu').addClass('div-view');
    });
    $('.close-menu').click(function() {
        $('.menu-box').hide();
        $('.in-box-menu').removeClass('div-view');
    });
    $('.ic-main-search').click(function() {
        $('.search-box').show();
    });
    $('.hide-search').click(function() {
        $('.search-box').hide();
    });
    $(window).scroll(function(){
        if ($(window).scrollTop() >= 200) {
            $('.fixed-social').addClass('fixed-icons');
        }
        else {
            $('.fixed-social').removeClass('fixed-icons');
        }
    });
    $('.ic-eye').on('click', function () {
        $('body').toggleClass('visually-impaired');
    });
    $('.video-slid').slick({
        //arrows: true,
        asNavFor: '.video-list',
        fade: true
    });
    $('.video-list').slick({
        asNavFor: '.video-slid',
        focusOnSelect: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        vertical: true,
        arrows: false
    });
    $('.photo-slid').slick({
        arrows: false,
        asNavFor: '.photo-list',
        fade: true,
        // vertical: true,
        // slidesToShow: 1,
        // slidesToScroll: 1,
    });
    $('.photo-list').slick({
        asNavFor: '.photo-slid',
        focusOnSelect: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        vertical: true,
        arrows: false
    });
    $('.gellery-item').slick({
        arrows: true,
        infinite: false
    });
    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
//                image: {
//                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
//                    titleSrc: function(item) {
//                        return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
//                    }
//                }
    });
});
$(document).ready(function() {
    $(window).scroll(function() {
        if ($(this).scrollTop() > 30) {
            $('.back-to-top').addClass('active');
            //$('#back-to-top').fadeIn();
        } else {
            $('.back-to-top').removeClass('active');
            //$('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('.back-to-top').click(function() {
        $('.back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('.back-to-top').tooltip('show');

});