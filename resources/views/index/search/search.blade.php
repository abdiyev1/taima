@extends('index.layout.layout')

@section('meta-tags')

    <title>{{Lang::get('app.search_result')}}</title>
    <meta name="description" content="{{Lang::get('app.search_result')}}"/>
    <meta name="keywords" content="{{Lang::get('app.search_result')}}"/>

@endsection

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row section-rows">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="/">{{Lang::get('app.homepage')}}</a></li>
                        <li class="active">{{Lang::get('app.search_result')}}</li>
                    </ol>
                    <div class="row">
                        <div class="col-md-9 col-sm-8">
                            @if($news_list->total() > 0)
                                <p class="title-page">{{Lang::get('app.found_by')}}: {{$news_list->total()}} {{Lang::get('app.material')}}</p>
                            @else
                                <p class="title-page">{{Lang::get('app.not_found')}}</p>
                            @endif

                            <div class="results-search">
                                @include('index.search.news-list-loop')
                            </div>
                            <div class="text-center">
                                {{ $news_list->appends(\Illuminate\Support\Facades\Input::except('page'))->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

