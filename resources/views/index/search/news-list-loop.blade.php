@foreach($news_list as $key => $item)

    <a href="{{$item->news_id}}-{{\App\Http\Helpers::getTranslatedSlugRu($item['news_name'])}}">
        <div class="item-results item-last-news clearfix">
            <div class="img-search">
                <img class="img-100" src="{{$item->news_image}}?width=295&height=200">
            </div>
            <div class="text-news-sch">
                <span class="time-audio-news">{{\App\Http\Helpers::getDateFormat($item->news_date)}}</span>
                <p>{{$item['news_name']}}</p>
                <span class="tags-news">{{$item['rubric_name']}}</span>
            </div>
        </div>
    </a>

@endforeach

