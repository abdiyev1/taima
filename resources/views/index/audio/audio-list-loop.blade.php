@foreach($audio_list as $key => $item)

    <div class="item-audio">
        <span class="time-audio-news">{{\App\Http\Helpers::getDateFormat($item->news_date)}}</span>
        <p><a>{{$item['news_name']}}</a></p>

        <audio controls>
            <source src="{{$item['news_desc']}}" type="audio/mp3">
        </audio>

        <input type="hidden" value="{{$item->news_date}}" class="news-item-hidden">
    </div>

@endforeach

@if(isset($is_ajax))
    <script>
        @if($audio_list->lastPage() > 1)
           $('#show_read_more').fadeIn(100);
        @else
           $('#show_read_more').css('display','none');
        @endif
    </script>
 @endif