@extends('index.layout.layout')

@section('meta-tags')

    <title>{{$rubric['rubric_meta_title_'.$lang]}}</title>
    <meta name="description" content="{{$rubric['rubric_meta_description_'.$lang]}}"/>
    <meta name="keywords" content="{{$rubric['rubric_meta_keywords_'.$lang]}}"/>

@endsection

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row section-rows">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="/">{{Lang::get('app.homepage')}}</a></li>
                        <li class="active">{{$rubric['rubric_name_'.$lang]}}</li>
                    </ol>
                    <div class="row">
                        <div class="col-md-9 col-sm-8">
                            <p class="title-page">{{$rubric['rubric_name_'.$lang]}}</p>
                            <form class="hidden" action="#" method="get">
                                <label>
                                    <select name="lang">
                                        <option value="ca">Català / Catalan (ca)</option>
                                    </select>
                                </label>
                                <label>
                                    <select name="stretching">
                                        <option value="auto" selected>Auto (default)</option>
                                    </select>
                                </label>
                            </form>
                            <div class="list-audio-page players" id="player2-container">
                                <div id="news_list_content">
                                    @include('index.audio.audio-list-loop')
                                </div>
                            </div>
                            <div class="text-center">
                                <button @if($audio_list->lastPage() <= 1) style="display: none" @endif class="btn btn-lg btn-blue" id="show_read_more" type="button" onclick="getNewsList('audio')">{{Lang::get('app.show_others')}}</button>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4">

                            <?php $banner = \App\Models\Banner::where('is_show',1)->where('position',1)->where('section_id',2)->orderBy('banner_id','desc')->first();?>
                            @if($banner != null)
                                <a @if($banner->website != '') href="{{$banner->website}}" @endif target="_blank">
                                    <img src="{{$banner->image}}?width=275&height=430" class="right-top-banner hidden-xs hidden-sm" >
                                </a>
                            @endif

                            <div class="last-news-box">
                                <div class="title-box">{{Lang::get('app.last_news')}}</div>

                                <div class="list-last-news">

                                    @include('index.news.latest-news-list-loop')

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="/js/player.js?v=2"></script>
@endsection