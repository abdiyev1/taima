<div class="col-md-8 col-xs-12">
    <p class="min-title-page comment-title">{{Lang::get('app.write_comment')}}</p>
    <div class="form-add-comments">
        <div class="row">
            <div class="col-sm-6">
                <input type="hidden" value="" id="answer_comment_id"/>
                <input class="form-control" type="text" placeholder="{{Lang::get('app.your_name')}}…" id="comment_user_name">
            </div>
            <div class="col-sm-6">
                <input class="form-control" type="email" placeholder="{{Lang::get('app.email')}}" id="comment_email">
            </div>
            <div class="col-xs-12">
                <textarea class="form-control" rows="8" placeholder="{{Lang::get('app.comment')}}…" id="comment_text"></textarea>
                <div class="text-right btns">
                    <button class="btn btn-white" type="button" value="{{Lang::get('app.cancel')}}" onclick="cancelWriteComment()">{{Lang::get('app.cancel')}}</button>
                    <button class="btn btn-blue" type="button" value="{{Lang::get('app.send')}}" onclick="addComment({{$news->news_id}})">{{Lang::get('app.send')}}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="list-comments" id="comment_list">
        @include('index.news.comment-list-loop')
    </div>
    <div class="text-center">
        <button @if($comment_list->lastPage() <= 1) style="display: none" @endif class="btn btn-lg btn-blue" id="show_read_more" type="button" onclick="getCommentList({{$news->news_id}})">{{Lang::get('app.show_others')}}</button>
    </div>
</div>