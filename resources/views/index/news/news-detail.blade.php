@extends('index.layout.layout')

@section('meta-tags')

    <title>{{$news['news_name_'.$lang]}}</title>
    <meta name="description" content="{{$news['news_meta_description_'.$lang]}}"/>
    <meta name="keywords" content="{{$news['news_meta_keywords_'.$lang]}}"/>
    <meta property="og:title" content="{{$news['news_name_'.$lang]}}" />
    <meta property="og:description" content="{{$news['news_meta_description_'.$lang]}}" />
    <meta property="og:url" content="{{Request::url()}}" />
    <meta property="og:image" content="{{URL('/').$news->news_image}}" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="400" />
    <meta property="og:image:height" content="300" />
    <link rel="image_src" href="{{URL('/').$news->news_image}}" />

@endsection

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row section-rows">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="/">{{Lang::get('app.homepage')}}</a></li>
                        <li><a href="/news">{{Lang::get('app.news')}}</a></li>
                        <li class="active">{{$news['news_name_'.$lang]}}</li>
                    </ol>
                    <div class="row">
                        <div class="col-md-9">
                            <p class="min-title-page">{{$news['news_name_'.$lang]}}</p>
                            <p class="time-news">
                                <span>{{\App\Http\Helpers::getDateFormat($news->news_date)}}</span>
                                <span><i class="icons ic-view"></i>{{$news->view_count}}</span>
                                <span><i class="icons ic-comments"></i>{{$news->comment_count}}</span>
                                <span><a class="blue-text">{{$news['author_'.$lang]}}</a></span>
                            </p>
                            <div class="in-news popup-gallery">
                               {!! $news['news_text_'.$lang] !!}
                            </div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox" style="margin-top: 60px"></div>
                            <!--<div class="social-likes">
                                <div data-service="facebook" title="Share link on Facebook"><span class="sm-hidden">Facebook</span></div>
                                <div data-service="twitter" title="Share link on Twitter"><span class="sm-hidden">Twitter</span></div>
                                <div data-service="plusone" title="Share link on Google+"><span class="sm-hidden">Google+</span></div>
                                <div data-service="pinterest" title="Share link on Pinterest" data-media="image link, required"><span class="sm-hidden">Pinterest</span></div>
                                <div data-service="vkontakte" title="Share link on Vkontakte"><span class="sm-hidden">Vkontakte</span></div>
                                <div data-service="telegram" title="Share link on Telegram"><span class="sm-hidden">Telegram</span></div>
                            </div>-->
                            <p class="tags">
                                <?php $tags = explode(",", $news['tag_'.$lang]);?>
                                @if(isset($tags[0]) && $tags[0] != '###')
                                    <span>Тегтер:</span>
                                    @foreach($tags as $item)
                                        <a href="/news?tag={{$item}}">{{$item}}</a>
                                    @endforeach
                                @endif
                            </p>
                            <div class="clearfix">
                                <p class="min-title-page">{{Lang::get('app.recommended_news')}}</p>
                                <div class="row list-box-news news">
                                   @include('index.news.similar-news-list-loop')
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">

                            <?php $banner = \App\Models\Banner::where('is_show',1)->where('position',1)
                                    ->where(function($query){
                                        $query->where('section_id',3)
                                        ->orWhere('section_id',2);
                                    })->orderBy('banner_id','desc')
                                    ->first();?>

                            @if($banner != null)
                                <a @if($banner->website != '') href="{{$banner->website}}" @endif target="_blank">
                                    <img src="{{$banner->image}}?width=275&height=430" class="right-top-banner hidden-xs hidden-sm" >
                                </a>
                            @endif

                            <div class="popular-news-box">
                                <div class="title-box">{{Lang::get('app.last_news')}}</div>
                                <div class="list-last-news">

                                    @include('index.news.latest-news-list-loop')

                                </div>
                            </div>
                            <?php $banner = \App\Models\Banner::where('is_show',1)->where('position',3)
                                    ->where(function($query){
                                        $query->where('section_id',3)
                                        ->orWhere('section_id',2);
                                    })->orderBy('banner_id','desc')
                                    ->first();?>

                            @if($banner != null)
                                <a @if($banner->website != '') href="{{$banner->website}}" @endif target="_blank">
                                    <img src="{{$banner->image}}?width=275&height=430" class="right-top-banner hidden-xs hidden-sm" >
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row section-rows">
                <div class="container">
                    <div class="row section-rows">
                        @include('index.news.comment-section')
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection




