@foreach($latest_news_list as $key => $item)
    <div class="item-last-news">
        <a href="/{{$item['rubric_url']}}"><small class="tags-news">{{$item['rubric_name']}}</small></a>
        <a href="{{$item->news_id}}-{{\App\Http\Helpers::getTranslatedSlugRu($item['news_name'])}}">
            <p>{{$item['news_name']}}</p>
        </a>
        <p class="time-news">
            <span>{{\App\Http\Helpers::getDateFormat($item->news_date)}}</span><span><i class="icons ic-view"></i>{{$item->view_count}}</span><span><i class="icons ic-comments"></i>{{$item->comment_count}}</span>
        </p>
    </div>

@endforeach