@foreach($news_list as $key => $item)
    <div class="col-sm-4">
        <a href="{{$item->news_id}}-{{\App\Http\Helpers::getTranslatedSlugRu($item['news_name'])}}">
            <div class="item-news">
                <div class="img-news tran-scale-img">
                    <img class="img-100" src="{{$item->news_image.'?width=295&height=200'}}@if($item->is_watermark == 1){{'&w=1'}}@endif">
                    @if($item->news_icon > 0)
                        <div class="icon-news ic-play-picture">
                            @if($item->news_icon == 1)
                                <i class="ic-picture"></i>
                            @elseif($item->news_icon == 2)
                                <i class="ic-video"></i>
                            @endif
                        </div>
                    @endif
                </div>
                <div class="text-news">
                    <p class="title-news">{{$item['news_name']}}</p>
                    <p class="time-news">
                        <span>{{\App\Http\Helpers::getDateFormat($item->news_date)}}</span><span><i class="icons ic-view"></i>{{$item->view_count}}</span><span><i class="icons ic-comments"></i>{{$item->comment_count}}</span>
                    </p>
                </div>
            </div>
            <input type="hidden" value="{{$item->news_date}}" class="news-item-hidden">
        </a>
    </div>

    {{--@if(($key + 1) % 3 == 0) <div class="clearfix"></div> @endif--}}

@endforeach

@if(isset($is_ajax))
    <script>
        @if($news_list->lastPage() > 1)
           $('#show_read_more').fadeIn(100);
        @else
           $('#show_read_more').css('display','none');
        @endif
    </script>
 @endif