@foreach($comment_list as $key => $item)

    <div class="item-comments">
        <div class="clearfix profile-comments">
            <img src="/img/icons/profile-user.png">
            <p class="name-comments">{{$item->comment_user_name}} @if($item->answer_comment_id > 0) <i>ответил(а)</i><a title="{{$item->answer_comment_text}}">{{$item->answer_comment_user_name}}</a> @endif <span>{{\App\Http\Helpers::getDateFormat($item->created_at)}}</span></p>
        </div>
        <div class="text-comments clearfix">
            <p>{{$item->message}}</p><a class="pull-right" href="javascript:void(0)" onclick="answerComment('{{$item->comment_id}}','{{$item->comment_user_name}}')">Ответить</a>
        </div>
        <input type="hidden" value="{{$item->comment_id}}" class="comment_id"/>
    </div>

@endforeach

@if(isset($is_ajax))
    <script>
        @if($comment_list->lastPage() > 1)
           $('#show_read_more').fadeIn(100);
        @else
           $('#show_read_more').css('display','none');
        @endif
    </script>
 @endif