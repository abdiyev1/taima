<?php $currency = \App\Models\Currency::where('currency_id','>',0)->first();?>
@if($currency != null)
    <ul class="info-course @if(isset($mobile_currency)) visible-xs @endif">
        <li class="currency_usd"><span>USD</span>{{$currency->usd}}</li>
        <li class="currency_eur"><span>EUR</span>{{$currency->euro}}</li>
        <li class="currency_rub"><span>RUB</span>{{$currency->rub}}</li>
    </ul>
@endif
