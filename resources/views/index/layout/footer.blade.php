<div class="footer">
    <div class="container-fluid">
        <div class="row top-footer">
            <div class="container">
                <div class="div-inline">
                    <div class="bottom-weather">
                        @include('index.layout.weather')
                    </div>
                    <div class="hidden-xs">
                        @include('index.layout.currency')
                    </div>
                </div>
                <form action="/search">
                    <input class="form-control search-bottom" name="q">
                </form>
            </div>
        </div>
        <div class="row section-rows">
            <div class="container">
                <div class="row center-footer">
                    <div class="col-sm-5 left-footer">
                        <div class="info-contacts">
                            <p class="blue-text" style="font-family: Arial;">{{Lang::get('app.we_in_social')}}:<a target="_blank" href="{{strip_tags(\App\Http\Helpers::getInfoText(2))}}"><i class="icons icon-social ic-ins"></i></a><a target="_blank" href="{{strip_tags(\App\Http\Helpers::getInfoText(4))}}"><i class="icons icon-social ic-tw"></i></a><a target="_blank" href="{{strip_tags(\App\Http\Helpers::getInfoText(3))}}"><i class="icons icon-social ic-fb"></i></a><a target="_blank" href="{{strip_tags(\App\Http\Helpers::getInfoText(1))}}"><i class="icons icon-social ic-vk"></i></a></p>
                            <p><i class="icons ic-email"></i><a class="blue-text" href="">etaimakz@gmail.com</a></p>
                            <p><i class="icons ic-phone"></i><a class="blue-text">8(725 40)5-55-77, 5-50-50</a></p>
                            <div class="copyright hidden-xs"><b>{!!Lang::get('app.reserved_title')!!}</b>
                                <p>{!!Lang::get('app.reserved')!!}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="row">
                            <div class="col-xs-6">
                                <ul class="footer-menu">
                                    <li class="title-menu">{{Lang::get('app.themes')}}</li>

                                    @foreach($main_rubric_list as $key => $item)

                                        <li class="@if(isset($menu) && $menu == $item['rubric_url_'.$lang]) active @endif">
                                            <a href="/{{strtolower($item['rubric_url_'.$lang])}}">{{$item['rubric_name_'.$lang]}}</a>
                                        </li>

                                    @endforeach

                                </ul>
                            </div>
                            <div class="col-xs-6">
                                <ul class="footer-menu">

                                    <li class="title-menu">{{Lang::get('app.about_us')}}</li>

                                    @foreach($main_page_list as $key => $item)

                                        <li class="@if(isset($menu) && $menu == $item['page_url_'.$lang]) title-menu @endif">
                                            <a href="/{{strtolower($item['page_url_'.$lang])}}">{{$item['page_name_'.$lang]}}</a>
                                        </li>

                                    @endforeach

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 right-footer">

                        <?php $journal = \App\Models\Journal::where('is_show',1)
                                            ->orderBy('journal_id','desc')
                                            ->first();?>

                        @if($journal != null)
                            <a target="_blank" href="{{$journal['journal_desc_'.$lang]}}">
                                <img src="{{$journal->journal_image}}?width=230&height=300"><span>{{$journal['journal_name_'.$lang]}}</span>
                            </a>
                        @endif

                    </div>
                    <div class="col-xs-12 visible-xs">
                        <div class="copyright"><b>{!!Lang::get('app.reserved_title')!!}</b>
                            <p>{!!Lang::get('app.reserved')!!}</p>
                        </div>
                    </div>
                </div>
                <div class="bottom-footer clearfix">
                    <p class="blue-text pull-left">{!!Lang::get('app.developer_company') !!}</p>
                    <div class="pull-right">
                        <span id="_zero_70047">
                            <noscript>
                            <a href="http://zero.kz/?s=70047" target="_blank">
                            <img src="http://c.zero.kz/z.png?u=70047" width="88" height="31" alt="ZERO.kz" />
                            </a>
                            </noscript>
                        </span>
                        <script type="text/javascript">
                            var _zero_kz_ = _zero_kz_ || [];
                            _zero_kz_.push(["id", 70047]);
                            _zero_kz_.push(["type", 1]);

                            (function () {
                                var a = document.getElementsByTagName("script")[0],
                                        s = document.createElement("script");
                                s.type = "text/javascript";
                                s.async = true;
                                s.src = (document.location.protocol == "https:" ? "https:" : "http:")
                                        + "//c.zero.kz/z.js";
                                a.parentNode.insertBefore(s, a);
                                console.log(a.parentNode);
                            })();
                        </script>
                        <!--LiveInternet counter--><script type="text/javascript">
                            document.write("<a href='//www.liveinternet.ru/click' "+
                                    "target=_blank><img src='//counter.yadro.ru/hit?t11.6;r"+
                                    escape(document.referrer)+((typeof(screen)=="undefined")?"":
                                    ";s"+screen.width+""+screen.height+""+(screen.colorDepth?
                                            screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
                                    ";h"+escape(document.title.substring(0,150))+";"+Math.random()+
                                    "' alt='' title='LiveInternet: показано число просмотров за 24"+
                                    " часа, посетителей за 24 часа и за сегодня' "+
                                    "border='0' width='88' height='31'><\/a>")
                        </script><!--/LiveInternet-->

                        <!-- Yandex.Metrika counter -->
                        <script type="text/javascript" >
                            (function (d, w, c) {
                                (w[c] = w[c] || []).push(function() {
                                    try {
                                        w.yaCounter47097975 = new Ya.Metrika({
                                            id:47097975,
                                            clickmap:true,
                                            trackLinks:true,
                                            accurateTrackBounce:true
                                        });
                                    } catch(e) { }
                                });

                                var n = d.getElementsByTagName("script")[0],
                                        s = d.createElement("script"),
                                        f = function () { n.parentNode.insertBefore(s, n); };
                                s.type = "text/javascript";
                                s.async = true;
                                s.src = "https://mc.yandex.ru/metrika/watch.js";

                                if (w.opera == "[object Opera]") {
                                    d.addEventListener("DOMContentLoaded", f, false);
                                } else { f(); }
                            })(document, window, "yandex_metrika_callbacks");
                        </script>
                        <noscript><div><img src="https://mc.yandex.ru/watch/47097975" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
                        <!-- /Yandex.Metrika counter -->

                        <script type="text/javascript" src="/orphus/orphus.js"></script>
                        <a href="https://orphus.ru" id="orphus" target="_blank" title="">
                            <img alt="Orphus system" src="/orphus/orphus.gif" border="0" width="197" height="39" />
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110852078-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-110852078-1');


</script>
