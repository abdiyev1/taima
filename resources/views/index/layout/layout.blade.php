<!DOCTYPE html>
<html>
@include('index.layout.app')

<?php $banner = \App\Models\Banner::where('is_show',1)
        ->where('position',5)
        ->where(function($query){ $query->where('section_id',1)->orWhere('section_id',2)->orWhere('section_id',3);})
        ->orderBy('banner_id','desc')->first();?>

<body @if(isset($_COOKIE['eye'])) class="visually-impaired" @endif @if($banner != null) style="background-image: url('{{$banner->image}}')" @endif>

<?php $banner = \App\Models\Banner::where('is_show',1)
        ->where('position',4)
        ->where(function($query){ $query->where('section_id',1)->orWhere('section_id',2)->orWhere('section_id',3);})
        ->orderBy('banner_id','desc')->first();?>

@if($banner != null)
    <div class="top-bunner">
        <div class="container">
            <a @if($banner->website != '') target="_blank" href="{{$banner->website}}" @endif>
                <img class="center-block bunner-top" src="{{$banner->image}}">
            </a>
        </div>
    </div>
@endif

<?php $main_rubric_list = \App\Models\Rubric::where('is_show_menu',1)->where('is_show',1)->orderBy('sort_num','asc')->get(); ?>
<?php $rubric_list = \App\Models\Rubric::where('is_show',1)->orderBy('sort_num','asc')->get(); ?>
<?php $main_page_list = \App\Models\Page::where('is_show_menu',1)->where('is_show',1)->orderBy('sort_num','asc')->get(); ?>

@include('index.layout.header')

@yield('content')

@include('index.layout.footer')

<script src="/js/lib.min.js?v=6"></script>
<script src="/custom/js/custom.js?v=7"></script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a582a0708e2736c"></script>

<script src="/js/bootstrap-select.js"></script>
<script>
    $('.selectpicker').selectpicker();
</script>

@yield('js')

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-message">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{Lang::get('app.contact_us')}}</h4>
            </div>
            <div class="modal-body">
                <input class="form-control" placeholder="{{Lang::get('app.your_name')}}*" type="text" id="user_name">
                <input class="form-control" placeholder="{{Lang::get('app.email')}}*" type="email" id="email">
                <input class="form-control" placeholder="{{Lang::get('app.message_title')}}*" type="text" id="contact_title">
                <textarea class="form-control" placeholder="{{Lang::get('app.write_us')}}*" rows="5" id="contact_message"></textarea>
                <div class="clearfix btns">
                    <button class="btn btn-white" type="button" value="" data-toggle="modal" data-target="#myModal">{{Lang::get('app.cancel')}}</button>
                    <button onclick="addContact()" class="btn btn-blue pull-right" type="button">{{Lang::get('app.send')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>

@if(!isset($menu) || $menu != 'news-detail')
    <div class="fixed-social">
        <div class="container">
            <div class="social-icons"><a target="_blank" href="https://api.whatsapp.com/send?phone=77013000577"><i class="icons ic-whatsapp"></i></a><a target="_blank" href="{{strip_tags(\App\Http\Helpers::getInfoText(2))}}"><i class="icons ic-ins"></i></a><a target="_blank" href="{{strip_tags(\App\Http\Helpers::getInfoText(4))}}"><i class="icons ic-tw"></i></a><a target="_blank" href="{{strip_tags(\App\Http\Helpers::getInfoText(3))}}"><i class="icons ic-fb"></i></a><a target="_blank" href="{{strip_tags(\App\Http\Helpers::getInfoText(1))}}"><i class="icons ic-vk"></i></a><a target="_blank" href="{{strip_tags(\App\Http\Helpers::getInfoText(11))}}"><i class="icons ic-ok"></i></a></div>
        </div>
    </div>
    {{--<div class="whatsaap-box">
        <b>WhatsApp</b>
        <a target="_blank" href="https://api.whatsapp.com/send?phone=77013000577">+7(701)300-05-77</a>
    </div>--}}
@endif

<a class=" back-to-top" id="back-to-top" href="#" role="" data-toggle="tooltip" data-placement="left"><img src="/img/icons/upload.png" alt=""></a>


</body>
</html>