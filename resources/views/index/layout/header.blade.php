<div class="header">
    <div class="container-fluid">
        <div class="row top-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-7">
                        <a><img class="ic-menu" src="/img/icons/ic-menu.png"></a>
                        <a href="/"><img class="logo" src="/img/logotype/logo.png"></a>
                    </div>
                    <div class="col-md-5 hidden-sm hidden-xs">
                        @include('index.layout.currency')
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-5 clearfix right-header">
                        <button class="btn btn-lg btn-blue hidden-xs" type="button" data-toggle="modal" data-target="#myModal">{{Lang::get('app.write_us')}}</button><i class="ic-main-search hidden-xs"></i><i class="ic-eye hidden-xs"></i><span class="change-lang dropdown">
                            <div class="dropdown-toggle" data-toggle="dropdown">{{Lang::get('app.lang')}}</div>
                                <ul class="dropdown-menu">
                                  <li><a href="{{\App\Http\Helpers::setSessionLang('kz',$request)}}" class="@if($lang == 'kz') active @endif">Қаз</a></li>
                                  <li><a href="{{\App\Http\Helpers::setSessionLang('ru',$request)}}" class="@if($lang == 'ru') active @endif">Рус</a></li>
                                  <li><a href="{{\App\Http\Helpers::setSessionLang('en',$request)}}" class="@if($lang == 'en') active @endif">Eng</a></li>
                                  <li><a href="{{\App\Http\Helpers::setSessionLang('qz',$request)}}" class="@if($lang == 'qz') active @endif">Qaz</a></li>
                                </ul>
                            </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row bottom-header hidden-xs" data-spy="affix" data-offset-top="200">
            <div class="container">
                <nav class="navbar">
                    <div class="navbar-header">
                        <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"><span class="sr-only"> Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li @if(isset($menu) && $menu == 'home') class="active" @endif>
                                <a href="/">{{Lang::get('app.homepage')}}</a>
                            </li>
                            <li @if(isset($menu) && $menu == 'news') class="active" @endif>
                                <a href="/news">{{Lang::get('app.news')}}</a>
                            </li>

                            @foreach($main_rubric_list as $key => $item)

                                <li class="@if(isset($menu) && $menu == $item['rubric_url_'.$lang]) active @endif">
                                    <a href="/{{strtolower($item['rubric_url_'.$lang])}}">{{$item['rubric_name_'.$lang]}}</a>
                                </li>

                            @endforeach
                        </ul>
                        <ul class="nav navbar-nav navbar-right hidden-sm">
                            <li class="top-weather">
                                @include('index.layout.weather')
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>

    <div class="in-box-menu">
        <div class="top-menu clearfix"><i class="icons close-menu"></i><img class="logo" src="/img/logotype/logo.png">
            <?php $mobile_currency = 1; ?>
            @include('index.layout.currency')
            <form action="/search">
                <input name="q" class="form-control search-bottom visible-xs" type="text">
            </form>
        </div>
        <div class="menu-left">
            <ul>
                <li @if(isset($menu) && $menu == 'home') class="active" @endif>
                    <a href="/">{{Lang::get('app.homepage')}}</a>
                </li>
                <li @if(isset($menu) && $menu == 'news') class="active" @endif>
                    <a href="/news">{{Lang::get('app.news')}}</a>
                </li>

                @foreach($rubric_list as $key => $item)

                    <li class="@if(isset($menu) && $menu == $item['rubric_url_'.$lang]) active @endif">
                        <a href="/{{strtolower($item['rubric_url_'.$lang])}}">{{$item['rubric_name_'.$lang]}}</a>
                    </li>

                @endforeach

            </ul>
        </div>
        <div class="bottom-menu">
            <ul>

                @foreach($main_page_list as $key => $item)

                    <li class="@if(isset($menu) && $menu == $item['page_url_'.$lang]) active @endif">
                        <a href="/{{strtolower($item['page_url_'.$lang])}}">{{$item['page_name_'.$lang]}}</a>
                    </li>

                @endforeach

            </ul>
        </div>
        <div class="left-footer">
            <div class="info-contacts">
                <p class="blue-text">{{Lang::get('app.we_in_social')}}:<a target="_blank" href="{{strip_tags(\App\Http\Helpers::getInfoText(2))}}"><i class="icons icon-social ic-ins"></i></a><a target="_blank" href="{{strip_tags(\App\Http\Helpers::getInfoText(4))}}"><i class="icons icon-social ic-tw"></i></a><a target="_blank" href="{{strip_tags(\App\Http\Helpers::getInfoText(3))}}"><i class="icons icon-social ic-fb"></i></a><a target="_blank" href="{{strip_tags(\App\Http\Helpers::getInfoText(1))}}"><i class="icons icon-social ic-vk"></i></a></p>
                <p><i class="icons ic-email"></i><a class="blue-text" href="">etaimakz@gmail.com</a></p>
                <p><i class="icons ic-phone"></i><a class="blue-text" href="">8(725 40)5-55-77, 5-50-50</a></p>
            </div>
        </div>
    </div>

<div class="search-box" @if(isset($_GET['q'])) style="display: block" @endif>
    <div class="container default-search">
        <div class="border-search">
            <div class="row">
                <div class="col-md-11">
                    <form action="/search">
                        <div class="input-group">
                                <input name="q" class="search-query form-control input-lg" type="text" value="@if(isset($_GET['q'])){{$_GET['q']}}@endif" placeholder="{{Lang::get('app.search')}}">
                                    <span class="input-group-btn">
                                    <button class="btn btn-lg btn-blue" type="submit">{{Lang::get('app.find')}}</button>
                                    </span>
                        </div>
                    </form>
                </div>
                <div class="col-md-1 text-center">
                    <i class="icons hide-search"></i>
                </div>
            </div>
        </div>
    </div>
</div>