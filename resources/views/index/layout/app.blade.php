<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/libs.min.css?v=22">
    <link rel="stylesheet" href="/css/main.min.css?v=24">
    <link rel="stylesheet" href="/css/bootstrap-select.css">
    <link rel="stylesheet" href="/custom/css/main.css?v=9">
    <link rel="icon" href="/img/icons/favicon.png">

    @yield('meta-tags')

</head>