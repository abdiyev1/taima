<?php $whether = \App\Models\Whether::where('id','>',0)->orderBy('city_id','asc')->get();
      $city = \App\Models\City::where('is_show',1)->orderBy('sort_num','asc')->get();
?>

<select class="city_list selectpicker" onchange="changeWhether(this)">
        @foreach($city as $key => $item)
                <option @if((!isset($_COOKIE['city_id']) && $key == 0 )|| (isset($_COOKIE['city_id']) && $_COOKIE['city_id'] == $item->city_id)) selected @endif value="{{$item->city_id}}">{{$item['city_name_'.$lang]}}</option>
        @endforeach
</select> @foreach($whether as $key => $item)<span @if((!isset($_COOKIE['city_id']) && $key == 0 )|| (isset($_COOKIE['city_id']) && $_COOKIE['city_id'] == $item->city_id)) style="display: inline-block" @endif class="whether-item whether_city_{{$item->city_id}}"><span>, {{\App\Http\Helpers::getCurrentDay()}}</span><i class="ic-weather" style="background-image:url('{{$item->image}}')"></i><span class="count-weather">{{$item->temperature}}°</span></span>@endforeach


