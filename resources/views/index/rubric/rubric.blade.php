@extends('index.layout.layout')

@section('meta-tags')

    <title>{{$rubric['rubric_meta_title_'.$lang]}}</title>
    <meta name="description" content="{{$rubric['rubric_meta_description_'.$lang]}}"/>
    <meta name="keywords" content="{{$rubric['rubric_meta_keywords_'.$lang]}}"/>

@endsection

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row section-rows">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="/">{{Lang::get('app.homepage')}}</a></li>
                        <li class="active">{{$rubric['rubric_name_'.$lang]}}</li>
                    </ol>
                    <div class="row">
                        <div class="col-md-9">
                            <p class="title-page">{{$rubric['rubric_name_'.$lang]}}</p>
                            <div class="row list-box-news news" id="news_list_content">

                                @include('index.news.news-list-loop')

                            </div>
                            <div class="text-center">
                                <button @if($news_list->lastPage() <= 1) style="display: none" @endif class="btn btn-lg btn-blue" id="show_read_more" type="button" onclick="getNewsList('{{$rubric['rubric_url_'.$lang]}}')">{{Lang::get('app.show_others')}}</button>
                            </div>
                        </div>
                        <div class="col-md-3">

                            <?php $banner = \App\Models\Banner::where('is_show',1)->where('position',1)->where('section_id',2)->orderBy('banner_id','desc')->first();?>
                            @if($banner != null)
                                <a @if($banner->website != '') href="{{$banner->website}}" @endif target="_blank">
                                    <img src="{{$banner->image}}?width=275&height=430" class="right-top-banner hidden-xs hidden-sm" >
                                </a>
                            @endif

                            <div class="last-news-box">
                                <div class="title-box">{{Lang::get('app.last_news')}}</div>
                                <div class="list-last-news">

                                    @include('index.news.latest-news-list-loop')

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

