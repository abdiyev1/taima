@extends('index.layout.layout')

@section('meta-tags')

    <title>{{strip_tags(\App\Http\Helpers::getInfoText(5))}}</title>
    <meta name="description" content="{{strip_tags(\App\Http\Helpers::getInfoText(6))}}" />
    <meta name="keywords" content="{{strip_tags(\App\Http\Helpers::getInfoText(7))}}" />

@endsection

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row section-rows">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">

                            @include('index.index.main-news')

                            <div class="row list-box-news">

                                @include('index.index.main-news-list-loop')

                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="last-news-box">
                                        <div class="title-box">{{Lang::get('app.last_news')}}</div>
                                        <div class="list-last-news">
                                            @include('index.news.latest-news-list-loop')
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <?php $banner = \App\Models\Banner::where('is_show',1)
                                                                        ->where('position',1)
                                                                        ->where(function($query){
                                                                            $query->where('section_id',1)
                                                                                    ->orWhere('section_id',2);
                                                                        })
                                                                        ->orderBy('banner_id','desc')
                                                                        ->first();?>
                                    @if($banner != null)
                                        <a @if($banner->website != '') href="{{$banner->website}}" target="_blank" @endif >
                                            <img src="{{$banner->image}}?width=275&height=430" class="right-top-banner hidden-xs" >
                                        </a>
                                    @endif
                                    <div class="popular-news-box">
                                        <div class="title-box">{{Lang::get('app.most_popular_news')}}</div>
                                        <div class="list-last-news">
                                            @include('index.index.popular-news-list-loop')
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php $banner = \App\Models\Banner::where('is_show',1)
                                    ->where('position',2)
                                    ->where(function($query){
                                        $query->where('section_id',1)
                                                ->orWhere('section_id',2);
                                    })
                                    ->orderBy('banner_id','desc')
                                    ->first();?>

                            @if($banner != null)
                                <a @if($banner->website != '') href="{{$banner->website}}" target="_blank" @endif >
                                    <img src="{{$banner->image}}?width=565&height=310" class="right-bottom-banner hidden-xs" >
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="container">
                  <div class="line-blue"></div>
                 </div>
            </div>
            <div class="row section-rows">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="title-box">{{Lang::get('app.video')}}</div>

                            <div class="clearfix video-block">
                                @include('index.index.video-news-list-loop')
                            </div>

                            <div class="title-box">{{Lang::get('app.photogallery')}}</div>
                            <div class="clearfix photo-block">
                                @include('index.index.photo-news-list-loop')
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="audio-box">
                                <div class="title-box">{{Lang::get('app.audio')}}</div>
                                <form class="hidden" action="#" method="get">
                                    <label>
                                        <select name="lang">
                                            <option value="ca">Català / Catalan (ca)</option>
                                        </select>
                                    </label>
                                    <label>
                                        <select name="stretching">
                                            <option value="auto" selected>Auto (default)</option>
                                        </select>
                                    </label>
                                </form>
                                <div class="list-audio players" id="player2-container">

                                    @include('index.index.audio-list-loop')

                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $banner = \App\Models\Banner::where('is_show',1)
                            ->where('position',3)
                            ->where(function($query){
                                $query->where('section_id',1)
                                        ->orWhere('section_id',2);
                            })
                            ->orderBy('banner_id','desc')
                            ->first();?>

                    @if($banner != null)
                        <a @if($banner->website != '') href="{{$banner->website}}" @endif target="_blank">
                            <img src="{{$banner->image}}?width=1150&height=100" class="center-block" >
                        </a>
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
  {{--  <script src="/js/player.js?v=2"></script>--}}
@endsection
