<div class="video-slid photo-gallery-boxes">

    @foreach($video_news_list as $key => $item)

        <a href="{{$item->news_id}}-{{\App\Http\Helpers::getTranslatedSlugRu($item['news_name'])}}">
            <div class="item-photo tran-scale-img">
                <img class="img-100" src="{{$item->news_image.'?width=470&height=305'}}@if($item->is_watermark == 1){{'&w=1'}}@endif">
                <div class="top-photo">
                    <p>{{$item['news_name']}}</p>
                    <p class="time-news">
                        <span>{{\App\Http\Helpers::getDateFormat($item->news_date)}}</span><span><i class="icons ic-view"></i>{{$item->view_count}}</span><span><i class="icons ic-comments"></i>{{$item->comment_count}}</span>
                    </p>
                </div>
            </div>
        </a>

    @endforeach

</div>
<div class="video-list list-photo-news">

    @foreach($video_news_list as $key => $item)

        <div class="item-photo clearfix">
            <div class="tran-scale-img">
                <img class="img-100" src="{{$item->news_image.'?width=215&height=125'}}@if($item->is_watermark == 1){{'&w=1'}}@endif">
            </div>
            <div class="text-news-photo"><span class="time-audio-news">{{\App\Http\Helpers::getDateFormat($item->news_date)}}</span>
                <p>{{$item['news_name']}}</p>
            </div>
        </div>

    @endforeach

</div>