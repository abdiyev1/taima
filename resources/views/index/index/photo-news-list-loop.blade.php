<div class="photo-slid photo-gallery-boxes">

    @foreach($photo_news_list as $key => $item)

        <div class="gellery-item">
            <div class="item-photo tran-scale-img">
                <a href="/{{$item->news_id}}-{{\App\Http\Helpers::getTranslatedSlugRu($item['news_name'])}}">
                    <img class="img-100" src="{{$item->news_image.'?width=470&height=305'}}@if($item->is_watermark == 1){{'&w=1'}}@endif">
                </a>
            </div>
            <?php
            $result = null;
            preg_match_all('/<img[^>]+>/i',$item->news_text, $result);
            ?>
            @if(isset($result[0]))
                @foreach($result[0] as $key => $image)
                    @if($key == 2) <?php break;?> @endif
                    <div class="item-photo tran-scale-img img-object-fit">
                        <a href="/{{$item->news_id}}-{{\App\Http\Helpers::getTranslatedSlugRu($item['news_name'])}}">
                            {!!$image!!}
                        </a>
                    </div>
                @endforeach
            @endif

            <div class="item-photo">
                <a href="/{{$item->news_id}}-{{\App\Http\Helpers::getTranslatedSlugRu($item['news_name'])}}">
                    <div class="img-min">
                        <img src="{{$item->news_image.'?width=470&height=305'}}@if($item->is_watermark == 1){{'&w=1'}}@endif">
                    </div>
                    <p class="end-box">Смотреть все фотографии</p>
                </a>
            </div>
        </div>

    @endforeach

</div>
<div class="photo-list list-photo-news">

    @foreach($photo_news_list as $key => $item)

        <div class="item-photo clearfix">
            <div class="tran-scale-img">
                <img class="img-100" src="{{$item->news_image.'?width=215&height=125'}}@if($item->is_watermark == 1){{'&w=1'}}@endif">
            </div>
            <div class="text-news-photo"><span class="time-audio-news">{{\App\Http\Helpers::getDateFormat($item->news_date)}}</span>
                <p>{{$item['news_name']}}</p>
            </div>
        </div>

    @endforeach

</div>