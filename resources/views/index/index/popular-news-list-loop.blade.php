@foreach($popular_news_list as $key => $item)
    <div class="item-last-news">
        <a href="{{$item->news_id}}-{{\App\Http\Helpers::getTranslatedSlugRu($item['news_name'])}}">
            <p>{{$item['news_name']}}</p>
        </a>
        <a><small class="tags-news">{{\App\Http\Helpers::getDateFormat($item->news_date)}}</small></a>
    </div>
@endforeach