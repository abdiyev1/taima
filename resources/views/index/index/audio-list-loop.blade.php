@foreach($audio_list as $key => $item)

    <div class="item-audio">
        <span class="time-audio-news">{{\App\Http\Helpers::getDateFormat($item->news_date)}}</span>
        <p><a>{{$item['news_name']}}</a></p>
        <audio controls>
            <source src="{{$item['news_desc']}}" type="audio/mp3">
        </audio>
    </div>

@endforeach

