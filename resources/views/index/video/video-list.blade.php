@extends('index.layout.layout')

@section('meta-tags')

    <title>{{$rubric['rubric_meta_title_'.$lang]}}</title>
    <meta name="description" content="{{$rubric['rubric_meta_description_'.$lang]}}"/>
    <meta name="keywords" content="{{$rubric['rubric_meta_keywords_'.$lang]}}"/>

@endsection

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row section-rows">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="/">{{Lang::get('app.homepage')}}</a></li>
                        <li class="active">{{$rubric['rubric_name_'.$lang]}}</li>
                    </ol>
                    <p class="title-page">{{$rubric['rubric_name_'.$lang]}}</p>
                    <div class="row list-box-news video-page" id="news_list_content">

                        @include('index.video.video-list-loop')

                    </div>
                    <div class="text-center">
                        <button @if($video_list->lastPage() <= 1) style="display: none" @endif class="btn btn-lg btn-blue" id="show_read_more" type="button" onclick="getNewsList('video')">Тағы да жүктеу</button>
                    </div>
                </div>
            </div>
            <div class="row section-rows">
                <div class="container">
                    <p class="min-title-page">{{Lang::get('app.recommended_news')}}</p>
                    <div class="row list-box-news news">
                        @include('index.video.popular-video-list-loop')
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

