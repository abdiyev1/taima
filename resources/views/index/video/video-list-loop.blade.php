@foreach($video_list as $key => $item)

    <div class="col-sm-4">
        <a href="{{$item->news_id}}-{{\App\Http\Helpers::getTranslatedSlugRu($item['news_name'])}}">
            <div class="item-news">
                <div class="img-news tran-scale-img">
                    <img class="img-100" src="{{$item->news_image}}?width=295&height=200">
                    <div class="top-play-ic"></div>
                </div>
                <div class="text-news">
                    <p class="title-news">{{$item['news_name']}}</p>
                    <p class="time-news">
                        <span>{{\App\Http\Helpers::getDateFormat($item->news_date)}}</span><span><i class="icons ic-view"></i>{{$item->view_count}}</span><span><i class="icons ic-comments"></i>{{$item->comment_count}}</span>
                    </p>
                </div>
            </div>
            <input type="hidden" value="{{$item->news_date}}" class="news-item-hidden">
        </a>
    </div>

@endforeach

@if(isset($is_ajax))
    <script>
        @if($video_list->lastPage() > 1)
           $('#show_read_more').fadeIn(100);
        @else
           $('#show_read_more').css('display','none');
        @endif
    </script>
 @endif