@extends('index.layout.layout')

@section('meta-tags')

    <title>{{$page['page_meta_title_'.$lang]}}</title>
    <meta name="description" content="{{$page['page_meta_description_'.$lang]}}"/>
    <meta name="keywords" content="{{$page['page_meta_keywords_'.$lang]}}"/>

@endsection

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row section-rows">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="/">{{Lang::get('app.homepage')}}</a></li>
                        <li class="active">{{$page['page_name_'.$lang]}}</li>
                    </ol>
                    <div class="row">
                        <div class="col-md-9">
                            <p class="min-title-page">{{$page['page_name_'.$lang]}}</p>
                            <div class="in-news">
                                {!! $page['page_text_'.$lang] !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <?php $banner = \App\Models\Banner::where('is_show',1)->where('position',1)->where('section_id',2)->orderBy('banner_id','desc')->first();?>
                            @if($banner != null)
                                <a @if($banner->website != '') href="{{$banner->website}}" @endif target="_blank">
                                    <img src="{{$banner->image}}?width=275&height=430" class="bunner-right" >
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

