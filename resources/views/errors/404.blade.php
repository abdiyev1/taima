@extends('index.layout.layout')


@section('meta-tags')

    <title>Ничего не найдено</title>

@endsection


@section('content')

    <div class="content">
        <div class="container">
            <div class="page-404">
                <p class="w-404">404</p>
                <p class="w-black">Бет табылмады</p>
                <p>Сіз іздеген ақпарат табылмады. Сайт бойынша іздеуді қолданып көріңіз немесе<a href="/">Басты бетке</a>өтіңіз. Сұрақтарыңыз бойынша<a href="/">бізге жазыңыз.</a></p>
            </div>
        </div>
    </div>

@endsection