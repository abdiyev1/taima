@extends('admin.layout.layout')

@section('content')

    <section class="content-header">
        <h1>
            {{ $title }}
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8" style="padding-left: 0px">
                    <div class="box box-primary">
                        @if (isset($error))
                            <div class="alert alert-danger">
                                {{ $error }}
                            </div>
                        @endif
                        @if($row->journal_id > 0)
                            <form action="/admin/journal/{{$row->journal_id}}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_method" value="PUT">
                                @else
                                    <form action="/admin/journal" enctype="multipart/form-data" method="post">
                                        @endif

                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input id="journal_id" type="hidden" name="journal_id" value="{{ $row->journal_id }}">
                                        <input type="hidden" class="image-name" id="journal_image" name="journal_image" value="{{ $row->journal_image }}"/>

                                        <div class="box-body">

                                            <div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#rus" data-toggle="tab">Русский</a>
                                                    </li>
                                                    <li>
                                                        <a href="#qaz" data-toggle="tab">Қазақша</a>
                                                    </li>
                                                    <li>
                                                        <a href="#eng" data-toggle="tab">English</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="active tab-pane" id="rus">
                                                        <div class="form-group">
                                                            <label>Заголовок (Рус)</label>
                                                            <input id="journal_name_ru" value="{{ $row->journal_name_ru }}" type="text" class="form-control" name="journal_name_ru" placeholder="Введите">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="file" value="@if(isset($row->journal_desc_ru)){{$row->journal_desc_ru}}@endif" name="journal_desc_ru"  onchange="uploadAudio('ru')"/>
                                                        </div>

                                                    </div>
                                                    <div class="tab-pane" id="qaz">
                                                        <div class="form-group">
                                                            <label>Заголовок (Каз)</label>
                                                            <input id="journal_name_kz" value="{{ $row->journal_name_kz }}" type="text" class="form-control" name="journal_name_kz" placeholder="Введите">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="file" value="@if(isset($row->journal_desc_kz)){{$row->journal_desc_kz}}@endif" name="journal_desc_kz" onchange="uploadAudio('kz')"/>
                                                        </div>

                                                    </div>
                                                    <div class="tab-pane" id="eng">
                                                        <div class="form-group">
                                                            <label>Заголовок (Англ)</label>
                                                            <input id="journal_name_en" value="{{ $row->journal_name_en }}" type="text" class="form-control" name="journal_name_en" placeholder="Введите">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="file" value="@if(isset($row->journal_desc_en)){{$row->journal_desc_en}}@endif" onchange="uploadAudio('en')" name="journal_desc_en"/>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>



                                        </div>

                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-primary">Сохранить</button>
                                        </div>
                                    </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box box-primary" style="padding: 30px; text-align: center">
                        <div style="padding: 20px; border: 1px solid #c2e2f0">
                            <img class="image-src" src="{{ $row->journal_image }}" style="width: 100%; "/>
                        </div>
                        <div style="background-color: #c2e2f0;height: 40px;margin: 0 auto;width: 2px;"></div>
                        <form id="image_form" enctype="multipart/form-data" method="post" class="image-form">
                            <i class="fa fa-plus"></i>
                            <input id="avatar-file" type="file" onchange="uploadImage()" name="image"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

