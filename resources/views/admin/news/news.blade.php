@extends('admin.layout.layout')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title box-title-first">
                        <a href="/admin/news" class="menu-tab @if(!isset($request->active) || $request->active == '1') active-page @endif">Активные новости</a>
                    </h3>
                    <h3 class="box-title box-title-second" >
                        <a href="/admin/news?active=0" class="menu-tab @if($request->active == '0') active-page @endif">Неактивные новости</a>
                    </h3>
                    <a href="/admin/news/create" style="float: right">
                        <button class="btn btn-primary box-add-btn">Добавить новость</button>
                    </a>
                    <div class="clear-float"></div>
                </div>
                <div>
                    <div style="text-align: left" class="form-group col-md-6" >

                        @if($request->active == '0')

                            <h4 class="box-title box-edit-click">
                                <a href="javascript:void(0)" onclick="isShowEnabledAll('news')">Сделать активным отмеченные</a>
                            </h4>

                        @else

                            <h4 class="box-title box-edit-click">
                                <a href="javascript:void(0)" onclick="isShowDisabledAll('news')">Сделать неактивным отмеченные</a>
                            </h4>

                        @endif


                    </div>
                    <div style="text-align: right" class="form-group col-md-6" >
                        <h4 class="box-title box-delete-click">
                            <a href="javascript:void(0)" onclick="deleteAll('news')">Удалить отмеченные</a>
                        </h4>
                    </div>
                </div>
                <div class="box-body">
                    <table id="news_datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr style="border: 1px">
                                <th style="width: 30px">№</th>
                                <th>Картинка</th>
                                <th>Название (ru)</th>
                                <th>Название (kz)</th>
                                <th>Рубрика</th>
                                <th>Позиция</th>
                                <th>Дата</th>
                                <th style="width: 15px"></th>
                                <th style="width: 15px"></th>
                                <th class="no-sort" style="width: 0px; text-align: center; padding-right: 16px; padding-left: 14px;" >
                                    <input onclick="selectAllCheckbox(this)" style="font-size: 15px" type="checkbox" value="1"/>
                                </th>
                            </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <form>
                                    <input value="{{$request->news_name}}" type="text" class="form-control" name="news_name" placeholder="Поиск">
                                    <input value="{{$request->rubric_name}}" type="hidden" class="form-control" name="rubric_name" placeholder="Поиск">
                                    <input value="{{$request->position_name}}" type="hidden" class="form-control" name="position_name" placeholder="Поиск">
                                    <input type="hidden" value="@if(!isset($request->active)){{'1'}}@else{{$request->active}}@endif" name="active"/>
                                </form>
                            </td>
                            <td>

                            </td>
                            <td>
                                <form>
                                    <input value="{{$request->news_name}}" type="hidden" class="form-control" name="news_name" placeholder="Поиск">
                                    <input value="{{$request->rubric_name}}" type="text" class="form-control" name="rubric_name" placeholder="Поиск">
                                    <input value="{{$request->position_name}}" type="hidden" class="form-control" name="position_name" placeholder="Поиск">
                                    <input type="hidden" value="@if(!isset($request->active)){{'1'}}@else{{$request->active}}@endif" name="active"/>
                                </form>
                            </td>
                            <td>
                                <form>
                                    <input value="{{$request->news_name}}" type="hidden" class="form-control" name="news_name" placeholder="Поиск">
                                    <input value="{{$request->rubric_name}}" type="hidden" class="form-control" name="rubric_name" placeholder="Поиск">
                                    <input value="{{$request->position_name}}" type="text" class="form-control" name="position_name" placeholder="Поиск">
                                    <input type="hidden" value="@if(!isset($request->active)){{'1'}}@else{{$request->active}}@endif" name="active"/>
                                </form>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                        @foreach($row as $key => $val)

                            <tr>
                                <td> {{ $key + 1 }}</td>
                                <td>
                                    <div class="object-image">
                                        <a class="fancybox" href="{{$val->news_image}}@if($val->is_watermark == 1){{'?w=1'}}@endif">
                                            <img src="{{$val->news_image}}">
                                        </a>
                                    </div>
                                    <div class="clear-float"></div>
                                </td>
                                <td>
                                    <a target="_blank" href="/{{$val->news_id}}-{{\App\Http\Helpers::getTranslatedSlugRu($val->news_name_ru)}}">
                                        {{ $val['news_name_ru']}}
                                    </a>
                                </td>
                                <td>
                                    {{ $val['news_name_kz']}}
                                </td>
                                <td>
                                    <? $news_rubric = \App\Models\NewsRubric::LeftJoin('rubric','rubric.rubric_id','=','news_rubric.rubric_id')
                                                                            ->select('rubric.*')
                                                                            ->where('news_id','=',$val->news_id)->get(); ?>

                                    @foreach($news_rubric as $key =>$value)
                                            @if($key != 0) {{', '}}@endif
                                            {{$value->rubric_name_ru}}
                                    @endforeach
                                </td>
                                <td>
                                    <? $news_position = \App\Models\NewsPosition::LeftJoin('position','position.position_id','=','news_position.position_id')
                                            ->select('position.*')
                                            ->where('news_id','=',$val->news_id)->get(); ?>

                                    @foreach($news_position as $key =>$value)
                                        @if($key != 0) {{', '}}@endif
                                        {{$value->position_name_ru}}
                                    @endforeach
                                </td>
                                <td>
                                    {{ $val['date']}}
                                </td>
                                <td style="text-align: center">
                                    <a href="javascript:void(0)" onclick="delItem(this,'{{ $val->news_id }}','news')">
                                        <li class="fa fa-trash-o" style="font-size: 20px; color: red;"></li>
                                    </a>
                                </td>
                                <td style="text-align: center">
                                    <a href="/admin/news/{{ $val->news_id }}/edit">
                                        <li class="fa fa-pencil" style="font-size: 20px;"></li>
                                    </a>
                                </td>
                                <td style="text-align: center;">
                                    <input class="select-all" style="font-size: 15px" type="checkbox" value="{{$val->news_id}}"/>
                                </td>
                            </tr>

                        @endforeach

                        </tbody>

                    </table>

                    <div style="text-align: center">
                        {{ $row->appends(\Illuminate\Support\Facades\Input::except('page'))->links() }}
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection