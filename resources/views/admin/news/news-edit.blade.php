@extends('admin.layout.layout')

@section('content')

    <section class="content-header">
        <h1>
            {{ $title }}
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8" style="padding-left: 0px">
                    <div class="box box-primary">
                        @if (isset($error))
                            <div class="alert alert-danger">
                                {{ $error }}
                            </div>
                        @endif
                        @if($row->news_id > 0)
                            <form action="/admin/news/{{$row->news_id}}" method="POST">
                                <input type="hidden" name="_method" value="PUT">
                                @else
                                    <form action="/admin/news" method="POST">
                                        @endif

                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input id="news_id" type="hidden" name="news_id" value="{{ $row->news_id }}">
                                        <input type="hidden" class="image-name" id="news_image" name="news_image" value="{{ $row->news_image }}"/>

                                        <div class="box-body">

                                            <div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#rus" data-toggle="tab">Русский</a>
                                                    </li>
                                                    <li>
                                                        <a href="#qaz" data-toggle="tab">Қазақша</a>
                                                    </li>
                                                    <li>
                                                        <a href="#eng" data-toggle="tab">English</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="active tab-pane" id="rus">
                                                        <div class="form-group">
                                                            <label>Заголовок (Рус)</label>
                                                            <input id="news_name_ru" value="{{ $row->news_name_ru }}" type="text" class="form-control" name="news_name_ru" placeholder="Введите">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Текст (Рус)</label>
                                                            <textarea id="news_text_ru" name="news_text_ru" class="ckeditor form-control text_editor2"><?=$row->news_text_ru?></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Тэги (через запятую)</label>
                                                            <textarea name="tag_ru" class="form-control"><?=$row->tag_ru?></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Meta description (Рус)</label>
                                                            <textarea name="news_meta_description_ru" class="form-control"><?=$row->news_meta_description_ru?></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Meta keywords (Рус)</label>
                                                            <textarea name="news_meta_keywords_ru" class="form-control"><?=$row->news_meta_keywords_ru?></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Автор (Рус)</label>
                                                            <input value="@if($row->author_ru != ''){{$row->author_ru}}@else{{Auth::user()->name}}@endif" type="text" class="form-control" name="author_ru" placeholder="Введите">
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="qaz">
                                                        <div class="form-group">
                                                            <label>Заголовок (Каз)</label>
                                                            <input id="news_name_kz" value="{{ $row->news_name_kz }}" type="text" class="form-control" name="news_name_kz" placeholder="Введите">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Текст (Каз)</label>
                                                            <textarea style="width: 100%" id="news_text_kz" name="news_text_kz" class="ckeditor form-control text_editor2"><?=$row->news_text_kz?></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Тэги (через запятую)</label>
                                                            <textarea name="tag_kz" class="form-control"><?=$row->tag_kz?></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Meta description (Каз)</label>
                                                            <textarea name="news_meta_description_kz" class="form-control"><?=$row->news_meta_description_kz?></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Meta keywords (Каз)</label>
                                                            <textarea name="news_meta_keywords_kz" class="form-control"><?=$row->news_meta_keywords_kz?></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Автор (Каз)</label>
                                                            <input value="{{ $row->author_kz }}" type="text" class="form-control" name="author_kz" placeholder="Введите">
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="eng">
                                                        <div class="form-group">
                                                            <label>Заголовок (Англ)</label>
                                                            <input id="news_name_en" value="{{ $row->news_name_en }}" type="text" class="form-control" name="news_name_en" placeholder="Введите">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Текст (Англ)</label>
                                                            <textarea style="width: 100%" id="news_text_en" name="news_text_en" class="ckeditor form-control text_editor2"><?=$row->news_text_en?></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Тэги (через запятую)</label>
                                                            <textarea name="tag_en" class="form-control"><?=$row->tag_en?></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Meta description (Англ)</label>
                                                            <textarea name="news_meta_description_en" class="form-control"><?=$row->news_meta_description_en?></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Meta keywords (Англ)</label>
                                                            <textarea name="news_meta_keywords_en" class="form-control"><?=$row->news_meta_keywords_en?></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Автор (Англ)</label>
                                                            <input value="{{ $row->author_en }}" type="text" class="form-control" name="author_en" placeholder="Введите">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label><a style="color: #3C8DBC">Рубрика</a></label>

                                                <div class="rubric-list">
                                                    <?foreach($rubric as $value){
                                                    $selected = '';

                                                    $news_rubric_count = \App\Models\NewsRubric::where('news_id','=',$row->news_id)->where('rubric_id','=',$value->rubric_id)->count();

                                                    if($news_rubric_count > 0){
                                                        $selected=' checked ';
                                                    }?>

                                                    <div class="left-float" style="width: 25%">
                                                        <div class="left-float delivery-checkbox">
                                                            <input name="rubric_list[]" {{ $selected }} type="checkbox" value="{{ $value->rubric_id }}"/>
                                                        </div>
                                                        <div class="left-float">
                                                            <span>{{ $value->rubric_name_ru }}</span>
                                                        </div>
                                                        <div class="clear-float"></div>
                                                    </div>
                                                    <?}?>
                                                    <div class="clear-float"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label><a href="javascript:void(0)" >Позиция</a></label>

                                                <div class="rubric-list">
                                                    <?foreach($position as $value){
                                                    $selected = '';

                                                    $news_position_count = \App\Models\NewsPosition::where('news_id','=',$row->news_id)->where('position_id','=',$value->position_id)->count();

                                                    if($news_position_count > 0){
                                                        $selected=' checked ';
                                                    }?>

                                                    <div class="left-float" style="width: 100%">
                                                        <div class="left-float delivery-checkbox">
                                                            <input name="position_list[]" {{ $selected }} type="checkbox" value="{{ $value->position_id }}"/>
                                                        </div>
                                                        <div class="left-float">
                                                            <span>{{ $value->position_name_ru }}</span>
                                                        </div>
                                                        <div class="clear-float"></div>
                                                    </div>
                                                    <?}?>
                                                    <div class="clear-float"></div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Иконка</label></br>
                                                <div class="variant-class">
                                                    <input @if($row->news_icon == '1') {{ ' checked ' }}  @endif type="radio" name="news_icon" value="1">  <i class="fa fa-camera"></i>
                                                    <input @if($row->news_icon == '2') {{ ' checked ' }} @endif type="radio" name="news_icon" value="2">  <i class="fa fa-video-camera"></i>
                                                    <input @if($row->news_icon == '0' || $row->news_icon == '') {{ ' checked ' }} @endif type="radio" name="news_icon" value="0" style="margin-left: 10px">  Нет
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Добавить водяной знак</label></br>
                                                <div class="variant-class">
                                                    <input @if($row->is_watermark == '0') {{ ' checked ' }}  @endif type="radio" name="is_watermark" value="0">  Нет
                                                    <input @if($row->is_watermark == '1') {{ ' checked ' }} @endif type="radio" name="is_watermark" value="1">  Да
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Дата</label>
                                                <input id="news_date" value="{{ $row->news_date }}" type="text" class="form-control datetimepicker-input" name="news_date" placeholder="Введите">
                                            </div>

                                        </div>

                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-primary">Сохранить</button>
                                        </div>
                                    </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box box-primary" style="padding: 30px; text-align: center">
                        <div style="padding: 20px; border: 1px solid #c2e2f0">
                            <img class="image-src" src="{{ $row->news_image }}" style="width: 100%; "/>
                        </div>
                        <div style="background-color: #c2e2f0;height: 40px;margin: 0 auto;width: 2px;"></div>
                        <form id="image_form" enctype="multipart/form-data" method="post" class="image-form">
                            <i class="fa fa-plus"></i>
                            <input id="avatar-file" type="file" onchange="uploadImage()" name="image"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

