@extends('admin.layout.layout')

@section('content')

    <section class="content-header">
        <h1>
            {{ $title }}
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8" style="padding-left: 0px">
                    <div class="box box-primary">
                        @if (isset($error))
                            <div class="alert alert-danger">
                                {{ $error }}
                            </div>
                        @endif
                        @if($row->news_id > 0)
                            <form action="/admin/audio/{{$row->news_id}}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_method" value="PUT">
                                @else
                                    <form action="/admin/audio" enctype="multipart/form-data" method="post">
                                        @endif

                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input id="news_id" type="hidden" name="news_id" value="{{ $row->news_id }}">
                                        <input type="hidden" class="image-name" id="news_image" name="news_image" value="{{ $row->news_image }}"/>

                                        <div class="box-body">

                                            <div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#rus" data-toggle="tab">Русский</a>
                                                    </li>
                                                    <li>
                                                        <a href="#qaz" data-toggle="tab">Қазақша</a>
                                                    </li>
                                                    <li>
                                                        <a href="#eng" data-toggle="tab">English</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="active tab-pane" id="rus">
                                                        <div class="form-group">
                                                            <label>Заголовок (Рус)</label>
                                                            <input id="news_name_ru" value="{{ $row->news_name_ru }}" type="text" class="form-control" name="news_name_ru" placeholder="Введите">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="file" value="@if(isset($row->news_desc_ru)){{$row->news_desc_ru}}@endif" name="news_desc_ru"  onchange="uploadAudio('ru')"/>
                                                        </div>
                                                        @if($row->news_id > 0)
                                                            <div class="form-group" id="ru_audio_content">
                                                                <audio controls>
                                                                    <source id="file_src_ru" src="@if(isset($row->news_desc_ru)){{$row->news_desc_ru}}@endif" type="audio/mp3">
                                                                </audio>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="tab-pane" id="qaz">
                                                        <div class="form-group">
                                                            <label>Заголовок (Каз)</label>
                                                            <input id="news_name_kz" value="{{ $row->news_name_kz }}" type="text" class="form-control" name="news_name_kz" placeholder="Введите">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="file" value="@if(isset($row->news_desc_kz)){{$row->news_desc_kz}}@endif" name="news_desc_kz" onchange="uploadAudio('kz')"/>
                                                        </div>
                                                        @if($row->news_id > 0)
                                                            <div class="form-group"id="kz_audio_content">
                                                                <audio controls>
                                                                    <source id="file_src_kz" src="@if(isset($row->news_desc_kz)){{$row->news_desc_kz}}@endif" type="audio/mp3">
                                                                </audio>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="tab-pane" id="eng">
                                                        <div class="form-group">
                                                            <label>Заголовок (Англ)</label>
                                                            <input id="news_name_en" value="{{ $row->news_name_en }}" type="text" class="form-control" name="news_name_en" placeholder="Введите">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="file" value="@if(isset($row->news_desc_en)){{$row->news_desc_en}}@endif" onchange="uploadAudio('en')" name="news_desc_en"/>
                                                        </div>
                                                        @if($row->news_id > 0)
                                                            <div class="form-group" id="en_audio_content">
                                                                <audio controls>
                                                                    <source id="file_src_en" src="@if(isset($row->news_desc_en)){{$row->news_desc_en}}@endif" type="audio/mp3">
                                                                </audio>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Дата</label>
                                                <input id="news_date" value="{{ $row->news_date }}" type="text" class="form-control datetimepicker-input" name="news_date" placeholder="Введите">
                                            </div>

                                        </div>

                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-primary">Сохранить</button>
                                        </div>
                                    </form>
                    </div>
                </div>

            </div>
        </div>
    </section>


@endsection

