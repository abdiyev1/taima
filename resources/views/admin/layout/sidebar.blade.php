<ul class="sidebar-menu">
  <li class="header">МЕНЮ</li>
  <li class="treeview @if(isset($menu) && $menu == 'news') active @endif" >
    <a href="/admin/news">
      <i class="fa fa-list-ul"></i>
      <span>Новости</span>
    </a>
  </li>
  <li class="treeview @if(isset($menu) && $menu == 'audio') active @endif" >
    <a href="/admin/audio">
      <i class="fa fa-list-ul"></i>
      <span>Аудио</span>
    </a>
  </li>
  <li class="treeview @if(isset($menu) && $menu == 'journal') active @endif" >
    <a href="/admin/journal">
      <i class="fa fa-list-ul"></i>
      <span>Газета</span>
    </a>
  </li>
  <li class="treeview @if(isset($menu) && $menu == 'comment') active @endif">
    <a href="/admin/comment?active=0">
      <i class="fa fa-comment-o"></i>
      <span>Комментарий</span>
      <? $count = \App\Models\Comment::where('is_show','=','0')->count();?>
      <span class="label label-primary pull-right notice-icon" @if($count > 0) style="display: block" @endif id="comment_count">{{$count}}</span>
    </a>
  </li>
  <li class="treeview @if(isset($menu) && $menu == 'contact') active @endif">
    <a href="/admin/contact?active=0">
      <i class="fa fa-comment-o"></i>
      <span>Обратная связь</span>
      <? $count = \App\Models\Contact::where('is_show','=','0')->count();?>
      <span class="label label-primary pull-right notice-icon" @if($count > 0) style="display: block" @endif id="contact_count">{{$count}}</span>
    </a>
  </li>
  <li class="treeview @if(isset($menu) && $menu == 'banner') active @endif" >
    <a href="/admin/banner">
      <i class="fa fa-list-ul"></i>
      <span>Баннер</span>
    </a>
  </li>
  <li class="treeview @if(isset($menu) && ($menu == 'rubric' || $menu == 'page')) active @endif">
    <a href="#">
      <i class="fa fa-file-text"></i>
      <span>Справочник</span>
      <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
      <li class="@if(isset($menu) && $menu == 'rubric') active @endif">
        <a href="/admin/rubric">
          <i class="fa fa-list"></i>
          <span>Рубрика</span>
        </a>
      </li>
      <li class="@if(isset($menu) && $menu == 'page') active @endif">
        <a href="/admin/page">
          <i class="fa fa-list"></i>
          <span>Статичные страницы</span>
        </a>
      </li>
      <li class="@if(isset($menu) && $menu == 'info') active @endif">
        <a href="/admin/info">
          <i class="fa fa-list"></i>
          <span>Статичные тексты</span>
        </a>
      </li>
    </ul>
  </li>
  <li class="treeview @if(isset($menu) && ($menu == 'admin' || $menu == 'moderator')) active @endif">
    <a href="#">
      <i class="fa fa-file-text"></i>
      <span>Пользователи</span>
      <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
      <li class="@if(isset($menu) && $menu == 'admin') active @endif">
        <a href="/admin/user">
          <i class="fa fa-user"></i>
          <span>Администратор</span>
        </a>
      </li>
      <li class="@if(isset($menu) && $menu == 'moderator') active @endif">
        <a href="/admin/moderator">
          <i class="fa fa-user"></i>
          <span>Модератор</span>
        </a>
      </li>
    </ul>
  </li>

  <li class="@if(isset($menu) && $menu == 'password') active @endif">
    <a href="/admin/password">
      <i class="fa fa-user"></i>
      <span>Сменить пароль</span>
    </a>
  </li>
  <li class="treeview">
    <a href="/logout">
      <i class="fa fa-sign-out"></i>
      <span>Выйти</span>
    </a>
  </li>
</ul>